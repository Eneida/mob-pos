import 'dart:async';
import 'package:mobpos/config.dart';
import 'package:dio/dio.dart';
import '../helpers/http_jwt.dart' as httpJwt;
import 'package:flutter/material.dart';

class ReportUuidService{
  static final ReportUuidService _instance = new ReportUuidService._internal();

  factory ReportUuidService(){
    return _instance;
  }

  ReportUuidService._internal();

  final String simpleReportUuid = Config.url_base_simple + '/v1/movements';
//  final String simpleReportUuid = Config.url_base_simple + '/v1/movements?limit=30&initDate=2019-05-01';

  Future<Response> queryReportUuid(BuildContext context, String size, String date) async{
    var response = await httpJwt.get(context, Uri.encodeFull('$simpleReportUuid?limit=$size&initDate=$date'));
    return response;
  }
}