import 'dart:async';
import 'package:dio/dio.dart';
import 'package:mobpos/config.dart';
import 'package:flutter/material.dart';
import '../helpers/http_jwt.dart' as httpJwt;

class ValidateStateCollectService{

  static final ValidateStateCollectService _instance = new ValidateStateCollectService._internal();

  factory ValidateStateCollectService() {
    return _instance;
  }

  ValidateStateCollectService._internal();

  final String validateCollectUrl = Config.url_base + '/ms-simple/v1/pos/validate?id=';

  Future<Response> queryValidateStateCollect(BuildContext context, int id) async {
    var json = {
      'id': id
    };
    print('Datos hacia el servicio::: $json');

    var response = await httpJwt.get(context, validateCollectUrl+'$id', headers: {"Content-Type": "application/json",
      "accept": "application/json"});
    print('Response validate collect:::: $response');
    return response;
  }
}