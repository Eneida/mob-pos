import 'dart:async';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:mobpos/config.dart';
import 'package:simple_logger/simple_logger.dart';
import '../helpers/http_jwt.dart' as httpJwt;

class MovementsService{

  final logger = SimpleLogger()..mode = LoggerMode.print;

  static final MovementsService _instance = new MovementsService._internal();

  factory MovementsService(){
    return _instance;
  }

  MovementsService._internal();

  final String movementUrl = Config.url_base_simple + '/v1/movements/last?offset=0';

  Future<Response> queryMovements(BuildContext context, String size, String date) async {
    logger.info('url movimientos: $movementUrl&size=$size&date=$date');

    var response = await httpJwt.get(context, '$movementUrl&size=$size&date=$date', headers: {"Content-Type": "application/json",
      "accept": "application/json"});
    print('Response movements:::: $response');
    return response;
  }
}