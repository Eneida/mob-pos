import 'dart:async';
import 'package:flutter/material.dart';
import 'package:mobpos/config.dart';
import 'package:dio/dio.dart';

class RegisterPosService{
  static final RegisterPosService _instance = new RegisterPosService._internal();

  factory RegisterPosService() {
    return _instance;
  }

  RegisterPosService._internal();

  final String registerPosUrl = Config.url_base + '/auth';

  Future<Response> queryRegisterPos(BuildContext context, String holderName, String company, String mobileNumber) async{

    Response response;
    try {
      response = await Dio().post(
          Uri.encodeFull(registerPosUrl),
          data: {"holderName": holderName, "company": company, 'mobile': mobileNumber},
          options: Options(headers: {"Content-Type": "application/json", "accept": "application/json",})
      );
    print('Respuesta registro service:::: $response');
    } catch (e) {
      print('Error response::: $e');
    }

    return response;
  }
}