import 'dart:async';
import 'package:dio/dio.dart';
import 'package:mobpos/config.dart';
import '../helpers/http_jwt.dart' as httpJwt;
import 'package:flutter/material.dart';

class SimpleQrCollectService{

  static final SimpleQrCollectService _instance = new SimpleQrCollectService._internal();

  factory SimpleQrCollectService() {
    return _instance;
  }

  SimpleQrCollectService._internal();

  final String collectQrUrl = Config.url_base + '/ms-simple/v1/pos/encrypt';

  Future<Response> queryCollectQr(BuildContext context, String account,
      int currency, String amount, String description, String expireDate,
      int singleUse) async {
    var json = {
      'account': account,
      'currency': currency,
      'amount': amount,
      'description': description,
      'expireDate': expireDate,
      'singleUse': singleUse
    };
    print('Datos hacia el servicio::: $json');

    var response = await httpJwt.post(context, collectQrUrl, json, headers: {"Content-Type": "application/json",
      "accept": "application/json"});
    print('Response collect:::: $response');
    return response;
  }
}