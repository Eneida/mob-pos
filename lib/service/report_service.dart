import 'dart:async';
import 'package:flutter/material.dart';

import '../config.dart';

class ReportService{
  static final ReportService _instance = new ReportService._internal();

  factory ReportService(){
    return _instance;
  }

  ReportService._internal();

  final String simpleReport = Config.url_base_simple + '/v1/movements/report/';

  Future<String> querySimpleReport(BuildContext context, String uuid)async{
    String urlReport = '$simpleReport$uuid';
    return urlReport;
  }
}
