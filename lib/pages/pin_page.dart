import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mobpos/config.dart';
import 'package:passcode_screen/passcode_screen.dart';

class PinPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _PinPage();
  }
}

class _PinPage extends State<PinPage>{

  @override
  void dispose() {
    _verificationNotifier.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PasscodeScreen(
        title: Text('Introdusca pin'),
        passwordEnteredCallback: _onPasscodeEntered,
        cancelButton: Text('Cancel'),
        deleteButton: Text('Delete'),
        shouldTriggerVerification: _verificationNotifier.stream,
      ),
    );
  }

  final StreamController<bool> _verificationNotifier = StreamController<bool>.broadcast();

  _onPasscodeEntered(String enteredPasscode) {
    bool isValid = '123456' == enteredPasscode;
    _verificationNotifier.add(isValid);
    print('Pass valido');
    Navigator.pushReplacementNamed(context, Config.home_page);
  }
}

// class CircleUIConfig {
//   final Color borderColor;
//   final Color fillColor;
//   final double borderWidth;
//   final double circleSize;
//   double extraSize;
// }
//
// class KeyboardUIConfig {
//   final double digitSize;
//   final TextStyle digitTextStyle;
//   final TextStyle deleteButtonTextStyle;
//   final Color primaryColor;
//   final Color digitFillColor;
//   final EdgeInsetsGeometry keyboardRowMargin;
//   final EdgeInsetsGeometry deleteButtonMargin;
// }