//import 'package:flutter/material.dart';
//import 'package:mobpos/config.dart';
//import 'package:mobpos/dto/product_dto.dart';
//import 'package:mobpos/scoped_models/mainModel.dart';
//import 'package:mobpos/themes/custom_theme.dart';
//import 'package:mobpos/ui/loading.dart';
//import 'package:mobpos/ui/widget_customized/account_origin_customized.dart';
//import 'package:mobpos/ui/widget_customized/buttons_customized.dart';
//import 'package:mobpos/ui/widget_customized/counter_button.dart';
//import 'package:mobpos/ui/widget_customized/scaffold_customized.dart';
//import 'package:scoped_model/scoped_model.dart';
//
//import '../pocket_localizations.dart';
//
//class CollectList extends StatefulWidget {
//  @override
//  State<StatefulWidget> createState() {
//    return _CollectListState();
//  }
//}
//
//class _CollectListState extends State<CollectList> {
//
//  Future<List<String>> loadData() async {
//    await Future.delayed(Duration(seconds: 3));
//    return List.generate(20, (index) => "$index").toList();
//  }
//
//  @override
//  void initState() {
//    var model = MainModel.of(context);
//    model.ctmLoadOriginAccount(context, true);
//    super.initState();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return ScopedModelDescendant<MainModel>(
//        builder: (BuildContext context, Widget child, MainModel model) {
//      return ScaffoldCustomized(
//          appBar: AppBar(
//            title: Text('Lista de productos'),
//          ),
//          body: Column(
//            children: <Widget>[
//              SizedBox(height: 8.0,),
//              AccountOriginCustomized(
//                  '${PocketLocalizations.of(context).accountThePayment}',
//                  model.getOriginAccountSelected,
//                  model.getOriginAccountsDropDown,
//                  model,
//                  PocketLocalizations.of(context).selectAccountPayment
//              ),
//              Expanded(child: _buildProductList(model, context)),
//              Container(
//                padding: EdgeInsets.all(10.0),
//                child: Column(
//                  children: <Widget>[
//                    ButtonTheme(
//                      shape: OutlineInputBorder(
//                        borderRadius: BorderRadius.circular(10.0),
//                      ),
//                      child: ButtonOutLine(
//                        textButton: 'Cobrar',
//                        submitForm: () {
//                          Navigator.pushNamed(context, Config.collect_page);
//                        },
//                      ),
//                    )
//                  ],
//                ),
//              )
//            ],
//          ));
//    });
//  }
//
//  Widget _buildProductList(MainModel model, BuildContext context) {
//    var itemTransaction;
//    if (model.productList == null) {
//      itemTransaction = Loading('Cargando sus productos');
//    } else if (model.productList.length > 0) {
//      itemTransaction = ListView(
//        children: [
//          new Padding(
//            padding: EdgeInsets.all(10.0),
//            child: ExpansionPanelList(
//              expansionCallback: (int index, bool isExpanded) {
//                setState(() {
//                  model.productList[index].isExpanded =
//                      !model.productList[index].isExpanded;
//                });
//              },
//              children:
//                  model.productList.map<ExpansionPanel>((ProductDto item) {
//                return ExpansionPanel(
//                  headerBuilder: (BuildContext context, bool isExpanded) {
//                    return ListTile(
//                        title: Row(
//                          mainAxisAlignment: MainAxisAlignment.spaceAround,
//                          children: <Widget>[
//                            Text(
//                              'Importe'
//                            ),
//                            Text(
//                              'Bs. ${item.totalAmount.toString()}',
//                              textAlign: TextAlign.left,
//                              style: TextStyle(
//                                fontSize: 20.0,
//                                fontWeight: FontWeight.w400,
//                              ),
//                            ),
//                          ],
//                        ));
//                  },
//                  isExpanded: item.isExpanded,
//                  body: Container(
//                    child: Column(
//                      children: <Widget>[
//                        Container(
//                          padding: const EdgeInsets.symmetric(horizontal: 16.0),
//                          child: TextFormField(
//                            autofocus: false,
//                            initialValue: item.description,
//                            decoration: InputDecoration(
//                              labelText: 'Descripción'
//                            ),
//                          ),
//                        ),
//                        GestureDetector(
//                          child: Container(
//                            padding: const EdgeInsets.symmetric(horizontal: 16.0),
//                            child: Column(
//                              mainAxisAlignment: MainAxisAlignment.start,
//                              children: <Widget>[
//
//                               // Expanded(
//                               //   child:
//
//                                // TextFormField(
//                                //   enabled: false,
//                                //   //textAlign: TextAlign.right,
//                                //   style: TextStyle(
//                                //     fontSize: 20.0,
//                                //   ),
//                                //   initialValue: item.totalAmount.toString(),
//                                //   decoration: InputDecoration(
//                                //       hintStyle: TextStyle(
//                                //           fontSize: 20.0,
//                                //       ),
//                                //     labelText: 'Importe',
//                                //   ),
//                                // ),
//                               // ),
//                              ],
//                            ),
//                          ),
//                          onTap: (){
//                            Navigator.pop(context);
//                          },
//                        ),
//                        Container(
//                          child: new Center(
//                            child: new Row(
//                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                              children: <Widget>[
//                                CounterButton(Icon(Icons.add, color: Colors.white,), (){
//                                  item.count ++;
//                                  item.totalAmount = item.amount * item.count;
//                                  model.totalAmount = model.totalAmount + item.amount;
//                                }),
//                                Text(item.count.toString(), style: TextStyle(fontSize: 20),),
//                                CounterButton(Icon(IconData(0xe15b, fontFamily: 'MaterialIcons'), color: Colors.white,),
//                                        (){
//                                  if(item.count != 0){
//                                    item.count --;
//                                    item.totalAmount = item.amount * item.count;
//                                    model.totalAmount = model.totalAmount + item.amount;
//                                  }
//                                        }),
//                              ],
//                            ),
//                          ),
//                        ),
//                        Divider(),
//                        Container(
//                          padding: const EdgeInsets.symmetric(vertical: 16.0),
//                          child: Row(
//                            mainAxisAlignment: MainAxisAlignment.end,
//                            children: <Widget>[
//                              Container(
//                                margin: const EdgeInsets.only(right: 8.0),
//                                child: FlatButton(
//                                  onPressed: (){},
//                                  child: const Text('CANCEL', style: TextStyle(
//                                    color: Colors.black54,
//                                    fontSize: 15.0,
//                                    fontWeight: FontWeight.w500,
//                                  )),
//                                ),
//                              ),
//                              Container(
//                                margin: const EdgeInsets.only(right: 8.0),
//                                child: FlatButton(
//                                  onPressed: (){},
//                                  textTheme: ButtonTextTheme.accent,
//                                  child: const Text('SAVE'),
//                                ),
//                              ),
//                            ],
//                          ),
//                        )
//                      ],
//                    ),
//                  ),
//                );
//              }).toList(),
//            ),
//          )
//        ],
//      );
//    } else {
//      itemTransaction = Center(
//        child: Text('No tiene productos'),
//      );
//    }
//    return itemTransaction;
//  }
//}
