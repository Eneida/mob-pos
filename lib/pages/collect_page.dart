import 'dart:async';
import 'package:flutter/material.dart';
import 'package:mobpos/pages/simple_detail_page.dart';
import 'package:mobpos/scoped_models/mainModel.dart';
import 'package:mobpos/themes/custom_theme.dart';
import 'package:mobpos/ui/widget_customized/buttons_customized.dart';
import 'package:mobpos/ui/widget_customized/card_customized.dart';
import 'package:mobpos/ui/widget_customized/scaffold_customized.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:screen/screen.dart';
import 'package:flutter_advanced_networkimage/provider.dart';

class CollectPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _CollectPageState();
  }
}

class _CollectPageState extends State with SingleTickerProviderStateMixin{

  AnimationController _controller;
  Animation _animation;
  Map<String,String> headersQr;

  @override
  void initState() {
    super.initState();
    var model = MainModel.of(context);
    print('pantalla de qr');
    _controller = AnimationController(duration: const Duration(seconds: 4), vsync: this);
    _animation = CurvedAnimation(parent: _controller, curve: Curves.easeIn);
    _controller.forward();

    Screen.keepOn(!model.isPayment);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
          return WillPopScope(
            onWillPop: (){
              model.destroyPayment();
              return Future.value(true);
            },
            child: ScaffoldCustomized(
              appBar: AppBar(
                title: Text('Cobrar'),
              ),
              body: ListView(
                children: <Widget>[
                  model.dataUser != null? _showDetailCollect(model): Container(),
                  _buildQr(model),
                  SizedBox(height: 5.0,),
                  //Se ocultara el boton de consultar pago si el qr no se carga o si el pago fue consolidado
                  !model.isPayment
                      ?_buildStatePayment(model)
                      : Container()
                ],
              ),
            ),
          );
        });
  }

  Widget _buildQr(MainModel model) {
//    this.headersQr = new Map<String,String>();
//    this.headersQr['Authorization'] = "Bearer ${model.tokenQr}";
    return Container(
      margin: EdgeInsets.all(10.0),
      child: CardCustomized(
        color: Color(0xFFE8EAF6),
        elev: 3,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Container(
              child: Text('Bs. ${model.totalAmount.toString()}',
                  style: TextStyle(fontSize: 38.0,
                      color: CustomTheme.getThemeData(context).getAccentColor(),
                      fontWeight: FontWeight.bold)),
            ),
            FadeTransition(
                opacity: _animation,
                child: Container(
                  height: 350,
                  width: 350,
                  child: true
                      ? Image.asset('assets/gracias.png')
                      : Image.asset('assets/qr_default.jpeg'),
//                      ? Image(
//                      image: AdvancedNetworkImage(
//                      model.getUrlImageSimple,
//                      header: this.headersQr,
//                      useDiskCache: true,
//                      ),)
//                      : Container(
//                    height: 350.0,
//                    child: Center(child: CircularProgressIndicator()),
//
//                  ),
    )
            ),
          ],
        ),
      ),
    );
  }

  Widget _showDetailCollect(MainModel model){
    return SimpleDetailPage(model);
  }

  Widget _buildStatePayment(MainModel model){
    return Container(
      child: Column(
        children: <Widget>[
          ButtonTheme(
            shape: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0),
            ),
            child: ButtonOutLine(
              textButton: 'Estado del pago',
              colorButton: CustomTheme.getThemeData(context).getAccentColor(),
              textSize: 20.0,
              submitForm: () {
                model.validateStateCollect(context, model.dataQrEncrypt.qrId);
              },
            ),
          )
        ],
      ),
    );
  }
  
  Timer timer;
  void payQrAfterTime(MainModel model){
    timer = Timer(const Duration(seconds: 10), (){
      model.searchItem(model.idLastTransfer.toString());
    });
  }

  void destroyCollect(){
    MainModel mainModel = (this as MainModel);

  }
}