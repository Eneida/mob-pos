import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:mobpos/helpers/dialog_util.dart';
import 'package:mobpos/pocket_localizations.dart';
import 'package:mobpos/scoped_models/mainModel.dart';
import 'package:mobpos/themes/custom_theme.dart';
import 'package:mobpos/ui/widget_customized/buttons_customized.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:scoped_model/scoped_model.dart';

import '../config.dart';

class WelcomePage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _WelcomePageState();
  }
}

class _WelcomePageState extends State<WelcomePage> with SingleTickerProviderStateMixin{

  Animation<double> animation;
  AnimationController controller;

  double size = 100.0;

  String dataQrRaw = '';

  @override
  void initState() {
    super.initState();
    controller = AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = Tween<double>(begin: 100, end: 300).animate(controller)
      ..addListener((){
        setState(() {
          size = animation.value;
        });
      });
    controller.forward();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ScopedModelDescendant<MainModel>(
          builder: (BuildContext context, Widget child, MainModel model) {
            return Container(
              padding: EdgeInsets.only(left: 24.0, right: 24.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  _buildLogo(),
                  Row(
                    children: <Widget>[
                      _buildWelcomeText(),
                    ],
                  ),
                  SizedBox(height: 100.0,),
                  _buildButtonStart(model, context)
                ],
              ),
            );
          })
    );
  }

  Widget _buildLogo() {
    return Container(
      height: size,
      width: size,
      margin: EdgeInsets.symmetric(vertical: 30.0),
      child: Image.asset('assets/logo-cirrus.png'),
      alignment: FractionalOffset.topCenter,
    );
  }

  Widget _buildWelcomeText() {
    return Container(
      child: Flexible(
        child: Center(
          child: Text('Bienvenido a ADA POS', style: TextStyle(
            color: CustomTheme.getThemeData(context).getAccentColor(),
            fontWeight: FontWeight.bold,
            fontSize: 35,
            letterSpacing: 2,
            fontStyle: FontStyle.italic
          ),
          textAlign: TextAlign.center,
          overflow: TextOverflow.clip,),
        ),
      ),
    );
  }

  Widget _buildButtonStart(MainModel model, BuildContext context){
    return ButtonOutLine(
      textButton: 'Empezar',
      submitForm: (){
        scan(model, context);
        Navigator.pushReplacementNamed(context, Config.register_page);
      },
    );
  }

  Future scan(MainModel model, BuildContext context) async {
    try {
      ScanResult barcode = await BarcodeScanner.scan();
        this.dataQrRaw = barcode.rawContent;
        print('qr leido: $barcode');
        //Guardando QR leido desde camara en el modelo
        model.tokenQr = dataQrRaw;
        print('---- 1 ----- $dataQrRaw');
        //se desencripto desde backend el qr leido anteriormente
        Navigator.pushReplacementNamed(context, Config.register_page);

    } on PlatformException catch (e) {
      print('---- 2 -----');
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        DialogUtil.showAcceptDialog(
            context,
            "Permiso Denegado",
            [Text('POS necesita acceder a la cámara para capturar códigos QR para ser autenticado.')],
            PocketLocalizations.of(context).accept);
        //Navigator.pushReplacementNamed(context, Config.menu_transfer_simple);*/
      } else {
        debugPrint('Desconocido error: $e');
        Navigator.pushReplacementNamed(context, Config.home_page);
      }
    } on FormatException {
      print('---- 3 -----');
      debugPrint('vacio (se retorno si leer nada)');
      //Navigator.pushReplacementNamed(context, Config.menu_transfers);

    } catch (e) {
      print('---- 4 -----');
      debugPrint('Error desconocido: $e');
      //alertDialog();
      //Navigator.pushReplacementNamed(context, Config.menu_transfer_simple);
    }
  }
}



