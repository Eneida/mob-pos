import 'package:flutter/material.dart';
import 'package:mobpos/config.dart';
import 'package:mobpos/helpers/secure_storage.dart';
import 'package:mobpos/scoped_models/mainModel.dart';
import 'package:mobpos/themes/custom_theme.dart';
import 'package:mobpos/ui/widget_customized/buttons_customized.dart';
import 'package:mobpos/ui/widget_customized/scaffold_customized.dart';
import 'package:toast/toast.dart';

class UnlinkDevice extends StatefulWidget{
  final MainModel model;

  UnlinkDevice(this.model);

  @override
  State<StatefulWidget> createState() {
    return _UnlinkDevice();
  }
}

class _UnlinkDevice extends State<UnlinkDevice>{
  @override
  Widget build(BuildContext context) {
    return ScaffoldCustomized(
      appBar: AppBar(title: Text('Desvincular dispositivo'),),
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Text('Desvincular dispositivo', style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
            Image.asset('assets/icons/out-line-icons/unlink-blue.png', scale: 0.7,),
            Text('Esta operación, desvinculara este dispositivo de la cuenta:', textAlign: TextAlign.center, style: TextStyle(fontSize: 17.0),),
            widget.model.dataUser != null?
            Column(
              children: <Widget>[
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Titular: ', style: TextStyle(fontSize: 17.0, fontWeight: FontWeight.w600),),
                    Text(widget.model.dataUser.name, style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w400),)
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Cuenta: ', style: TextStyle(fontSize: 17.0, fontWeight: FontWeight.w600),),
                    Text(widget.model.dataUser.ac, style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w400),)
                  ],
                ),
              ],
            ): Container(),
            ButtonTheme(
              shape: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: ButtonOutLine(
                textButton: 'Desvincular',
                colorButton: CustomTheme.getThemeData(context).getAccentColor(),
                textSize: 20.0,
                submitForm: () {
                  SecureStorage.delete(Config.jwt_token_key);
                  SecureStorage.delete(Config.user_name);
                  SecureStorage.delete(Config.user_account);
                  SecureStorage.delete(Config.user_ci);
                  SecureStorage.delete(Config.jwt_token_key);
                  MainModel.of(context).destroyNavigationModel();
                  Navigator.pushReplacementNamed(context, Config.welcome_page);
                  Toast.show('El dispositivo fue desvinculado', context, duration: Toast.LENGTH_LONG);
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}