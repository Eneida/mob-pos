import 'package:flutter/material.dart';
import 'package:mobpos/scoped_models/mainModel.dart';
import 'package:mobpos/ui/widget_customized/label_value_custom.dart';

class SimpleDetailPage extends StatelessWidget{
  final MainModel model;

  SimpleDetailPage(this.model);

@override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Text('Farmacias Chavez', style: TextStyle(fontWeight: FontWeight.w600), textAlign: TextAlign.center,),
          LabelValueCustom('Nro. Cuenta', '20202024034'),
        ],
      ),
    );
  }
}