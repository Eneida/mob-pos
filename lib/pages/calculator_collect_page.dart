import 'package:flutter/material.dart';
import 'package:mobpos/config.dart';
import 'package:mobpos/presentation/pos_app_icons.dart';
import 'package:mobpos/scoped_models/mainModel.dart';
import 'package:mobpos/themes/custom_theme.dart';
import 'package:mobpos/ui/widget_customized/buttons_customized.dart';
import 'package:mobpos/ui/widget_customized/calculator_button.dart';
import 'package:mobpos/ui/widget_customized/scaffold_customized.dart';
import 'package:scoped_model/scoped_model.dart';

class CalculatorCollectPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CalculatorCollectPageState();
  }
}

class _CalculatorCollectPageState extends State<CalculatorCollectPage> {
  bool savedLastValue = false;
  List _currentValues = List();
  double lastValue;
  GlobalKey<FormState> _formCalculatorKey = GlobalKey<FormState>();

  Key _sevenKey = Key('seven');
  Key _eightKey = Key('eight');
  Key _nineKey = Key('nine');
  Key _fourKey = Key('four');
  Key _fiveKey = Key('five');
  Key _sixKey = Key('six');
  Key _oneKey = Key('one');
  Key _twoKey = Key('two');
  Key _threeKey = Key('three');
  Key _zeroKey = Key('zero');
  Key _clearKey = Key('clear');
  Key _allClearKey = Key('allclear');

  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
      return ScaffoldCustomized(
        appBar: AppBar(
          title: Image.asset("assets/logo.png", height: 50),
          centerTitle: true,
        ),
        body: Container(
            child: Stack(
          fit: StackFit.expand,
          children: [
            Column(
              children: <Widget>[
                Expanded(
                  flex: 4,
                  child: _buildScreenCalculator(model),
                ),
                Expanded(
                  flex: 6,
                  child: _buildKeyboardCalculator(),
                ),
                _buildCollectButton(model)
              ],
            ),
          ],
        )),
      );
    });
  }

  Widget _buildScreenCalculator(MainModel model) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 8.0),
      child: Column(
        children: <Widget>[
          Expanded(
            child: Form(
              key: _formCalculatorKey,
              child: Container(
                child: IgnorePointer(
                  child: TextFormField(
                    enabled: true,
                    autofocus: false,
                    controller: model.amountController,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontSize: 70.0,
                    ),
                    decoration: InputDecoration.collapsed(
                        hintText: '0.00', hintStyle: TextStyle(fontSize: 70.0)),
                    // ignore: missing_return
                    validator: (String value) {
                      if (value.isEmpty || value == '0.00') {
                        return 'Importe requerido';
                      }
                    },
                    onSaved: (String value) {
                      model.totalAmount = value;
                    },
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildKeyboardCalculator() {
    return ListView(
      shrinkWrap: false,
      children: <Widget>[
        Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            CalculatorButton<String>(
              number: '1',
              keyNumber: _oneKey,
              keyTap: onKeyTapped,
              color: Colors.grey,
            ),
            CalculatorButton<String>(
              number: '2',
              keyNumber: _twoKey,
              keyTap: onKeyTapped,
              color: Colors.grey,
            ),
            CalculatorButton<String>(
              number: '3',
              keyNumber: _threeKey,
              keyTap: onKeyTapped,
              color: Colors.grey,
            ),
          ],
        ),
        Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            CalculatorButton<String>(
              number: '4',
              keyNumber: _fourKey,
              keyTap: onKeyTapped,
              color: Colors.grey,
            ),
            CalculatorButton<String>(
              number: '5',
              keyNumber: _fiveKey,
              keyTap: onKeyTapped,
              color: Colors.grey,
            ),
            CalculatorButton<String>(
              number: '6',
              keyNumber: _sixKey,
              keyTap: onKeyTapped,
              color: Colors.grey,
            ),
          ],
        ),
        Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            CalculatorButton<String>(
              number: '7',
              keyNumber: _sevenKey,
              keyTap: onKeyTapped,
              color: Colors.grey,
            ),
            CalculatorButton<String>(
              number: '8',
              keyNumber: _eightKey,
              keyTap: onKeyTapped,
              color: Colors.grey,
            ),
            CalculatorButton<String>(
              number: '9',
              keyNumber: _nineKey,
              keyTap: onKeyTapped,
              color: Colors.grey,
            ),
          ],
        ),
        Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            CalculatorButton<Icon>(
              number: Icon(
                PosApp.trash,
                color: Colors.red,
              ),
              keyNumber: _allClearKey,
              keyTap: onKeyTapped,
              color: Colors.red,
            ),
            CalculatorButton<String>(
              number: '0',
              keyNumber: _zeroKey,
              keyTap: onKeyTapped,
              color: Colors.grey,
            ),
            CalculatorButton<Icon>(
              number: Icon(
                PosApp.left_outline,
                color: Color(0xFFFBC02D),
              ),
              keyNumber: _clearKey,
              keyTap: onKeyTapped,
              color: Colors.green,
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildCollectButton(MainModel model) {
    return Column(
      children: <Widget>[
        ButtonTheme(
          shape: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: ButtonOutLine(
            textButton: 'Cobrar',
            colorButton: CustomTheme.getThemeData(context).getAccentColor(),
            textSize: 20.0,
            submitForm: () {
              if (!_formCalculatorKey.currentState.validate()) {
                return;
              }
              _formCalculatorKey.currentState.save();
//              model.qrCollect(context, model.dataUser.ac,  1, model.totalAmount, '', 'SEMANA', 1);
              Navigator.pushNamed(context, Config.collect_page);
              //limpiando datos al salir de la pantalla
              onKeyTapped(_allClearKey);
            },
          ),
        )
      ],
    );
  }

  void onKeyTapped(Key key) {
    var model = MainModel.of(context);
    if (savedLastValue == false && lastValue != null) {
      _currentValues.clear();
      savedLastValue = true;
    }
    if (identical(_sevenKey, key)) {
      _currentValues.add('7');
      model.amountController.text = convertToString(_currentValues);
      print('_currentValues: $_currentValues');
      print('amountController: ${model.amountController.text}');
    } else if (identical(_eightKey, key)) {
      _currentValues.add('8');
      model.amountController.text = convertToString(_currentValues);
    } else if (identical(_nineKey, key)) {
      _currentValues.add('9');
      model.amountController.text = convertToString(_currentValues);
    } else if (identical(_fourKey, key)) {
      _currentValues.add('4');
      model.amountController.text = convertToString(_currentValues);
    } else if (identical(_fiveKey, key)) {
      _currentValues.add('5');
      model.amountController.text = convertToString(_currentValues);
    } else if (identical(_sixKey, key)) {
      _currentValues.add('6');
      model.amountController.text = convertToString(_currentValues);
    } else if (identical(_oneKey, key)) {
      _currentValues.add('1');
      model.amountController.text = convertToString(_currentValues);
    } else if (identical(_twoKey, key)) {
      _currentValues.add('2');
      model.amountController.text = convertToString(_currentValues);
    } else if (identical(_threeKey, key)) {
      _currentValues.add('3');
      model.amountController.text = convertToString(_currentValues);
    } else if (identical(_zeroKey, key)) {
      _currentValues.add('0');
      model.amountController.text = convertToString(_currentValues);
    } else if (identical(_clearKey, key) && _currentValues.isNotEmpty) {
      print('Values :: $_currentValues');
       _currentValues.removeLast();
       model.amountController.text = convertToString(_currentValues);
    } else if (identical(_allClearKey, key)) {
      _currentValues.clear();
      _currentValues = List();
      lastValue = null;
      savedLastValue = false;
      model.amountController.updateValue(0.00);
    }
  }

  String convertToString(List values) {
    String val = '';
    print('currentValues convertToString $_currentValues');
    print('values.length ${values.length}');
    for (int i = 0; i < values.length; i++) {
      val += _currentValues[i];
    }
    print('val: $val');
    return val;
  }

  String validateDouble(double doubleValue) {
    int value;
    if (doubleValue % 1 == 0) {
      value = doubleValue.toInt();
    } else {
      return doubleValue.toStringAsFixed(1);
    }
    return value.toString();
  }

  List convertToList(String value) {
    List list = new List();
    for (int i = 0; i < value.length; i++) {
      list.add(String.fromCharCode(value.codeUnitAt(i)));
    }
    return list;
  }
}
