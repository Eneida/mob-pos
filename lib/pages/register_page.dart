import 'package:flutter/material.dart';
import 'package:mobpos/scoped_models/mainModel.dart';
import 'package:mobpos/themes/custom_theme.dart';
import 'package:mobpos/ui/widget_customized/buttons_customized.dart';
import 'package:mobpos/ui/widget_customized/texts_customized.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:scoped_model/scoped_model.dart';
import '../pocket_localizations.dart';

class RegisterPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _RegisterPage();
  }
}

class _RegisterPage extends State<RegisterPage>{
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final Map<String, dynamic> _formRegistration = {};
  final FocusNode _holderFocus = FocusNode();
  final FocusNode _companyFocus = FocusNode();
  final FocusNode _mobileFocus = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Registar nuevo POS'),),
      body: ScopedModelDescendant<MainModel>(
          builder: (BuildContext context, Widget child, MainModel model) {
            //model.setDeviceWidth = MediaQuery.of(context).size.width;
            var modalProgress =  ModalProgressHUD(
                child: Container(
                  padding: EdgeInsets.only(left: 24.0, right: 24.0),
                  child: Center(
                    child: SingleChildScrollView(
                      child: Container(
                          child: Theme(
                              data: CustomTheme.getThemeData(context).getLoginTheme(),
                              child: Form(
                                  key: _formKey,
                                  child: Column(children: <Widget>[
//                                    _buildLogo(),
//                                    _buildWelcomeNickname(model),
//                                    _buildWelcomePocket(),
                                    SizedBox(height: 48.0,),
                                    _buildHolderName(),
                                    SizedBox(height: 10.0),
                                    _buildCompanyName(),
                                    SizedBox(height: 20.0),
                                    _buildMobile(),
                                    SizedBox(height: 20.0),
                                    _buildRegisterButton(),
                                    SizedBox(height: 50.0),
//                                    _buildChannelsIcon(),
//                                    SizedBoxDefault(hD: 10.0,)
                                  ])))),
                    ),
                  ),
                ),
                inAsyncCall: model.progress,
                color: Colors.black,
                opacity: 0.4);
            return  modalProgress;
          }
      ),
    );
  }

  Widget _buildHolderName() {
    return TextFormDefault(
      labelTextForm: PocketLocalizations.of(context).holder,
      validatorText: PocketLocalizations.of(context).youMustEnterYourHolder,
      formToSave: _formRegistration,
      valueForm: 'holderName',
      focusNode: _holderFocus,
      textInputAction: TextInputAction.next,
      functionTextKeyBoard: (value){_holderFocus.unfocus(); FocusScope.of(context).requestFocus(_companyFocus);},);
  }
  Widget _buildCompanyName() {
    return TextFormDefault(
      labelTextForm: PocketLocalizations.of(context).company,
      validatorText: PocketLocalizations.of(context).youMustEnterYourCompany,
      formToSave: _formRegistration,
      valueForm: 'companyName',
      focusNode: _companyFocus,
      textInputAction: TextInputAction.next,
      functionTextKeyBoard: (value){_companyFocus.unfocus(); FocusScope.of(context).requestFocus(_mobileFocus);},);
  }
  Widget _buildMobile() {
    return TextFormDefault(
      labelTextForm: PocketLocalizations.of(context).mobileNumber,
      validatorText: PocketLocalizations.of(context).youMustEnterYourMobileNumber,
      formToSave: _formRegistration,
      valueForm: 'mobile',
      focusNode: _mobileFocus,
      textInputAction: TextInputAction.done,
      functionTextKeyBoard: (value){
        _mobileFocus.unfocus();
        registerPos();
        },);
  }

  Widget _buildRegisterButton() {
    return ButtonOutLine(
      textButton: 'Registrar',
      colorButton: CustomTheme.getThemeData(context).getAccentColor(),
      textSize: 30.0,
      submitForm: (){
        MainModel.of(context).loadFileSound();
        registerPos();
      },
    );
  }

  void registerPos(){
    // Si el formulario no es valido se termina la ejecucion
    FocusScope.of(context).requestFocus(new FocusNode());
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();
    MainModel.of(context).getRegisterPos(context, _formRegistration);
  }
}