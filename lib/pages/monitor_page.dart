import 'package:flutter/material.dart';
import 'package:mobpos/scoped_models/mainModel.dart';
import 'package:mobpos/ui/loading.dart';
import 'package:mobpos/ui/widget_customized/tile_monitor.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:firebase_database/firebase_database.dart';

class MonitorPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _MonitorPage();
  }
}

class _MonitorPage extends State<MonitorPage>{

  final DatabaseReference databaseReference = FirebaseDatabase.instance.reference().child("test");


  sendData(){
    databaseReference.push().set({
      'idTransfer': '765572',
      'origin': 'Carla',
      'amount': '3.00'
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Monitor'),),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Monitor'),
            SizedBox(height: 0.5,),
            Expanded(
              child: ScopedModelDescendant<MainModel>(
                builder: (BuildContext context, Widget child, MainModel model) {
                  return _buildMonitorList(model, context);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

 Widget _buildMonitorList(MainModel model, BuildContext context) {
   var itemTransaction;
   if (model.monitorList == null) {
     itemTransaction = Loading('Cargando sus pagos');
   } else if (model.monitorList.length > 0) {
     itemTransaction = ListView.builder(
       itemBuilder: (BuildContext context, int index) {
         return Column(
           children: <Widget>[
             TileMonitor(
               idTransfer: model.monitorList[index].idTransfer,
               amount: model.monitorList[index].amount,
               originName: model.monitorList[index].originName,
             ),
             Divider()
           ],
         );
       },
       itemCount: model.monitorList.length,
     );
   } else {
     itemTransaction = Center(
       child: Text('No tiene pagos'),
     );
   }
   return itemTransaction;
 }
}