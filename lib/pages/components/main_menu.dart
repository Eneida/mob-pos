import 'package:flutter/material.dart';
import 'package:mobpos/pages/calculator_collect_page.dart';
import 'package:mobpos/pages/movement_page.dart';
import 'package:mobpos/pages/unlink_device.dart';
import 'package:mobpos/ui/widget_customized/scaffold_customized.dart';
import 'package:scoped_model/scoped_model.dart';
import '../../scoped_models/mainModel.dart';
import '../../themes/custom_theme.dart';
import '../../pocket_localizations.dart';

class MainMenu extends StatefulWidget {
  @override
  _MainMenuState createState() => new _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {
  PageController _pageController;

  
  @override
  void initState() {
    super.initState();
    MainModel model = MainModel.of(context);
    model.saveDataUserModel();
    _pageController = PageController(
      initialPage: model.nmPage,
    );
    model.initNavigationModel(_pageController);
  }

  @override
  Widget build(BuildContext context) {
    return new ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
      return WillPopScope(
        // ignore: missing_return
        onWillPop: (){
          MainModel.of(context).destroyNavigationModel();
          model.initNavigationModel(_pageController);
          Future.value(true);
        },
        child: new ScaffoldCustomized(
          body: new PageView(
            controller: _pageController,
            onPageChanged: (newPage) => model.nmPage = newPage,
            children: <Widget>[
              CalculatorCollectPage(),
              MovementsPage(),
              UnlinkDevice(model)
            ],
          ),
          bottomNavigationBar: Theme(
            data: CustomTheme.getThemeData(context).getBottomBarTheme(),
            // sets the inactive color of the `BottomNavigationBar`
            child: new BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              currentIndex: model.nmPage,
              onTap: (index) => model.nmChangePage(index),
              selectedItemColor: CustomTheme.getThemeData(context).getAccentColor(),
              items: <BottomNavigationBarItem>[
                new BottomNavigationBarItem(
                    icon: ImageIcon(AssetImage('assets/icons/out-line-icons/pos-terminal.png')),
                    title: Text(PocketLocalizations.of(context).pos)),
                new BottomNavigationBarItem(
                  icon: ImageIcon(AssetImage('assets/icons/out-line-icons/history.png')),
                  title: Text(PocketLocalizations.of(context).movements),),
                new BottomNavigationBarItem(
                  icon: ImageIcon(AssetImage('assets/icons/out-line-icons/download-rep.png')),
                  title: Text(PocketLocalizations.of(context).reports, style: TextStyle(fontSize: 12.0),),),
              ],
            ),
          ),
        ),
      );
    });
  }
}
