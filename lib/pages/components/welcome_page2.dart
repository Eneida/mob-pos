import 'dart:ui';
import 'package:flutter/services.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter/cupertino.dart';
import 'package:mobpos/config.dart';
import 'package:mobpos/helpers/dialog_util.dart';
import 'package:mobpos/helpers/secure_storage.dart';
import 'package:mobpos/pocket_localizations.dart';
import 'package:mobpos/scoped_models/mainModel.dart';
import 'package:mobpos/ui/widget_customized/buttons_customized.dart';
import 'package:scoped_model/scoped_model.dart';

class WelcomePage2 extends StatefulWidget {
  @override
  _WelcomePage2State createState() => new _WelcomePage2State();
}

class _WelcomePage2State extends State<WelcomePage2> {
  String dataQrRaw = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ScopedModelDescendant<MainModel>(
            builder: (BuildContext context, Widget child, MainModel model) {
              return Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/background/pos_image.jpeg'),
                    colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.6), BlendMode.darken),
                    fit: BoxFit.fill,
                  ),
                ),
                child: Column(
                  children: <Widget>[
                    Flexible(
                      child: Stack(
                        children: <Widget>[
                          Swiper.children(
                            loop: false,
                            autoplay: false,
                            controller: model.swiperController,
                            pagination: SwiperPagination(
                                margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 30.0),
                                builder: DotSwiperPaginationBuilder(
                                    color: Colors.white30,
                                    activeColor: Colors.white,
                                    size: 10.0,
                                    activeSize: 15.0)),
                            children: listSwiper,
                            onIndexChanged: (index) {
                              if(index == 2) {
                                model.setSkip = true;
                              } else {
                                model.setSkip = false;
                              }
                            },
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          bottom: 30, left: 10, right: 10),
                      child:
                      model.getSkip?
                      ButtonOutLine(
                        textButton: 'Escanear QR',
                        colorButton: Colors.white,
                        textSize: 20.0,
                        submitForm: (){
                          scan(model, context);
                        },
                      ): Container(),
//                      FlatButton(
//                        child: Text(
//                          'Omitir',
//                          style: TextStyle(
//                              fontSize: 17.0,
//                              color: Colors.white,
//                              fontWeight: FontWeight.w400),
//                        ),
//                        onPressed: () {
//                          model.swiperController.move(2);
//                          model.setSkip = true;
//                        },
//                      ),
                    ),
                  ],
                ),
              );
            })
    );
  }

  List<Widget> listSwiper = [
    Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(vertical: 18.0),
            child: Image.asset('assets/logo_white.png'),
          ),
          Text('¡Bienvenido  \nPunto de Venta!', style: TextStyle(color: Colors.white, fontSize: 25.0, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
          SizedBox(height: 30.0,),
          Flexible(child: Text('En tu nueva App del Banco Fortaleza S.A. podras:', style: TextStyle(color: Colors.white, fontSize: 17.0, fontWeight: FontWeight.w500), textAlign: TextAlign.center, overflow: TextOverflow.clip,),),
          SizedBox(height: 20.0,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Column(
                children: <Widget>[
                  ImageIcon(AssetImage('assets/icons/out-line-icons/receive-cash.png'), color: Colors.white, size: 45.0,),
                  SizedBox(height: 5.0,),
                  Text('Recibir pagos', style: TextStyle(color: Colors.white), textAlign: TextAlign.center,),
                ],
              ),
              Column(
                children: <Widget>[
                  ImageIcon(AssetImage('assets/icons/out-line-icons/history2.png'), color: Colors.white, size: 45.0,),
                  SizedBox(height: 5.0,),
                  Text('Consultar\nmovimientos', style: TextStyle(color: Colors.white), textAlign: TextAlign.center,),
                ],
              ),
              Column(
                children: <Widget>[
                  ImageIcon(AssetImage('assets/icons/out-line-icons/notifications.png'), color: Colors.white, size: 45.0,),
                  SizedBox(height: 5.0,),
                  Text('Recibir\nnotificaciones', style: TextStyle(color: Colors.white), textAlign: TextAlign.center,),
                ],
              ),
            ],
          ),
          SizedBox(height: 15.0,),
          Text('de manera simple y rápida.', style: TextStyle(color: Colors.white, fontSize: 17.0, fontWeight: FontWeight.w500), textAlign: TextAlign.center,),
        ],
      ),
    ),
    Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Requisitos', style: TextStyle(color: Colors.white, fontSize: 25.0, fontWeight: FontWeight.w500), textAlign: TextAlign.center,),
          SizedBox(height: 15.0,),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text('Para continuar verifique que cuenta con los siguiente:', style: TextStyle(color: Colors.white, fontSize: 17.0, fontWeight: FontWeight.w500), textAlign: TextAlign.center,),
              SizedBox(height: 10.0,),
              ListTile(
                title: Text('Caja de Ahorro o Cuenta Corriente en el Banco Fortaleza', style: TextStyle(color: Colors.white, fontSize: 17.0, fontWeight: FontWeight.w500), textAlign: TextAlign.start,),
                leading: ImageIcon(AssetImage('assets/icons/out-line-icons/money-save.png'), color: Colors.white, size: 40.0,),
              ),
              ListTile(
                title: Text('Usuario y contraseña para acceder a tu banca por internet', style: TextStyle(color: Colors.white, fontSize: 17.0, fontWeight: FontWeight.w500), textAlign: TextAlign.start,),
                leading: ImageIcon(AssetImage('assets/icons/out-line-icons/phone-lock.png'), color: Colors.white, size: 40.0,),
              ),
              ListTile(
                title: Text('Un equipo con conexión a internet', style: TextStyle(color: Colors.white, fontSize: 17.0, fontWeight: FontWeight.w500), textAlign: TextAlign.start,),
                leading: ImageIcon(AssetImage('assets/icons/out-line-icons/woman-computer.png'), color: Colors.white, size: 40.0,),

              ),
            ],
          ),
        ],
      ),
    ),
    Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Instrucciones', style: TextStyle(color: Colors.white, fontSize: 25.0, fontWeight: FontWeight.w500), textAlign: TextAlign.center,),
          SizedBox(height: 15.0,),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ListTile(
                title: Text('Acceda a su Banca por Internet', style: TextStyle(color: Colors.white, fontSize: 17.0, fontWeight: FontWeight.w500), textAlign: TextAlign.start,),
                leading: Icon(Icons.check, color: Colors.white,),
              ),
              Column(
                children: <Widget>[
                  ListTile(
                    title: Text('En su computador ingrese a la siguiente dirección:\nhttps://www.bancofortaleza.com.bo/bancainternet/\ny busque la opción Punto de Venta', style: TextStyle(color: Colors.white, fontSize: 15.0, fontWeight: FontWeight.w500), textAlign: TextAlign.start,),
                    leading: Icon(Icons.check, color: Colors.white),
                  ),
                  Container(
                    padding: EdgeInsets.all(7.0),
                    child: Image.asset('assets/menu-web.png'),
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  ListTile(
                    title: Text('Y busque el código QR', style: TextStyle(color: Colors.white, fontSize: 17.0, fontWeight: FontWeight.w500), textAlign: TextAlign.start,),
                    leading: Icon(Icons.check, color: Colors.white),
                  ),
                  ImageIcon(AssetImage('assets/icons/out-line-icons/qr-code.png'), color: Colors.white, size: 45.0,),
                ],
              ),
              //   ImageIcon(AssetImage('assets/login/huella.png'), size: 75.0, color: Colors.white,),
            ],
          ),
        ],
      ),
    )
  ];

  Future scan(MainModel model, BuildContext context) async {
    try {
      ScanResult barcode = await BarcodeScanner.scan();
      this.dataQrRaw = barcode.rawContent;
      print('qr leido: $barcode');
      //Guardando QR leido desde camara en el modelo
      model.tokenQr = dataQrRaw;
      SecureStorage.write(Config.jwt_token_key, dataQrRaw);
      model.decodeDataQr(dataQrRaw);
      //se desencripto desde backend el qr leido anteriormente
      Navigator.pushReplacementNamed(context, Config.main_menu_page);

    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        DialogUtil.showAcceptDialog(
            context,
            "Permiso Denegado",
            [Text('POS necesita acceder a la cámara para capturar códigos QR para ser autenticado.')],
            PocketLocalizations.of(context).accept);
        //Navigator.pushReplacementNamed(context, Config.menu_transfer_simple);*/
      } else {
        debugPrint('Error: $e');
        Navigator.pushReplacementNamed(context, Config.home_page);
      }
    } on FormatException catch (e) {
      debugPrint('Exception scan $e');
    } catch (e) {
      debugPrint('Error desconocido: $e');
    }
  }
}
