import 'package:flutter/material.dart';
import 'package:mobpos/config.dart';
import 'package:mobpos/scoped_models/mainModel.dart';
import 'package:mobpos/themes/custom_theme.dart';
import 'package:mobpos/ui/widget_customized/scaffold_customized.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:mobpos/pocket_localizations.dart';

class HomePage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _HomePage();
  }
}

class _HomePage extends State<HomePage>{
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
          return ScaffoldCustomized(
            appBar: AppBar(title: Text(PocketLocalizations.of(context).pos)),
            body: Container(
              child: GridView.count(
                crossAxisCount: 2,
                childAspectRatio: 1.0,
                padding: EdgeInsets.all(8.0),
                mainAxisSpacing: 8.0,
                crossAxisSpacing: 8.0,
                children: <Widget>[
                  GestureDetector(
                    child: Container(
                      padding: EdgeInsets.all(5.0),
                      decoration: new BoxDecoration(
                        border: new Border.all(width: 2),
                        borderRadius: BorderRadius.all(Radius.circular(CustomTheme.getThemeData(context).getButtonRadius())),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          IconTheme(
                            data: IconThemeData(size: 65.0),
                            child: ImageIcon(AssetImage('assets/simple/read_qr.png')),
                          ),
                          SizedBox(height: 12.0),
                          Text('Cobrar POS'),
                        ],
                      ),
                    ),
                    onTap: (){
                      Navigator.pushNamed(context, Config.calculator_collect_page);
                    },
                  ),
                  GestureDetector(
                    child: Container(
                      padding: EdgeInsets.all(5.0),
                      decoration: new BoxDecoration(
                        border: new Border.all(width: 2),
                        borderRadius: BorderRadius.all(Radius.circular(CustomTheme.getThemeData(context).getButtonRadius())),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          IconTheme(
                            data: IconThemeData(size: 65.0),
                            child: Icon(Icons.compare_arrows),
                          ),
                          SizedBox(height: 12.0),
                          Text('Movimientos'),
                        ],
                      ),
                    ),
                    onTap: (){
                    },
                  ),
                  GestureDetector(
                    child: Container(
                      padding: EdgeInsets.all(5.0),
                      decoration: new BoxDecoration(
                        border: new Border.all( width: 2),
                        borderRadius: BorderRadius.all(Radius.circular(CustomTheme.getThemeData(context).getButtonRadius())),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          IconTheme(
                            data: IconThemeData(size: 65.0),
                            child: Icon(Icons.location_searching),
                          ),
                          SizedBox(height: 12.0),
                          Text('Monitor de pago'),
                        ],
                      ),
                    ),
                    onTap: (){
                      Navigator.pushNamed(context, Config.monitor_page);
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }
}