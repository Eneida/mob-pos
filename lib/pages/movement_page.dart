import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:mobpos/pocket_localizations.dart';
import 'package:mobpos/scoped_models/mainModel.dart';
import 'package:mobpos/ui/loading.dart';
import 'package:mobpos/ui/widget_customized/tile_movement.dart';
import 'package:scoped_model/scoped_model.dart';

class MovementsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MovementsStatePage();
  }
}

class _MovementsStatePage extends State<MovementsPage> {

  ScrollController _hideButtonController;
  var _isVisible;

  @override
  void initState() {
    var model = MainModel.of(context);
    model.movementList();
    model.extractMovement(context, '30', '');
    model.uuidMovements(context, '30', '');

    //Si el Scroll sube el boton se habilita y el scroll baja el control se oculta
    _isVisible = true;
    _hideButtonController = new ScrollController();
    _hideButtonController.addListener((){
      if(_hideButtonController.position.userScrollDirection == ScrollDirection.reverse){
        if(_isVisible == true) {
          /* only set when the previous state is false
             * Less widget rebuilds
             */
          setState((){
            _isVisible = false;
          });
        }
      } else {
        if(_hideButtonController.position.userScrollDirection == ScrollDirection.forward){
          if(_isVisible == false) {
            /* only set when the previous state is false
               * Less widget rebuilds
               */
            setState((){
              _isVisible = true;
            });
          }
        }
      }});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
          var itemTransaction;
          return Scaffold(
            appBar: AppBar(
              title: Text('Movimientos'),
              centerTitle: true,
            ),
            body: Center(
              child: _buildListExtract(model),),
            //Ocultamos el boton flotante cuando la lista es nula
            floatingActionButton: model.extractMovements != null
            //Acitvamos la visivilidad del boton de acuerdo al scrollController
                ? Visibility(
              visible: _isVisible,
              child: FloatingActionButton(
                  child: ImageIcon(AssetImage(
                      'assets/icons/out-line-icons/download-rep.png')),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20.0))),
                  onPressed: () {
                    model.reportMovements(context, model.getUuidSimple);
                  }),
            ) :Container(),
          );
        });
  }

  Widget _buildListExtract(MainModel model) {
    var itemTransaction;
    model.extractMovements == null
        ? itemTransaction = Loading(PocketLocalizations
        .of(context)
        .loadingYourTransactions) :
    model.extractMovements != null && model.extractMovements.length > 0 ?
    itemTransaction = CustomScrollView(
      controller: _hideButtonController,
      slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate((BuildContext context,
              int index) {
            bool last = model.extractMovements != null ? model.extractMovements
                .length == (index + 1) : false;
            if (!last) {
              return TileMovement(
                movements: model.extractMovements[index],
                model: model,
                index: index,
              );
            } else {
              return Column(
                children: <Widget>[
                  TileMovement(
                    movements: model.extractMovements[index],
                    model: model,
                    index: index,
                  ),
                  Container(
                    padding: last ? EdgeInsets.all(35.0) : null,
                    child: Container(),
                  ),
                ],
              );
            }
          },
              childCount: model.extractMovements.length
          ),
        )
      ],
    )
        : itemTransaction = Center(
      child: Text(PocketLocalizations
          .of(context)
          .hasNoExtractAvailable),
    );
    return itemTransaction;
  }
}
