import 'package:flutter/material.dart';

class Config {

  static const bool isProduction = false;
  static const String version = "0.0.0";
  static const String jwt_token_key = "JWT_TOKEN_KEY";
  static const String user_name = "USER_NAME";
  static const String user_account = "USER_ACCOUNT";
  static const String user_ci = "USER_CI";

  static const String url_base = "http://bfocore.cirrus-it.net:29990";            //TEST
//  static const String url_base = "http://dev.pocketbank.io:8282";            //TEST
  static const String url_base_simple = url_base + "/ms-simple";             //SIMPLE

  // Backgroun scaffold
  static const Color backgroundColor = Color(0xFF252F34);
  static const double overlay = 0.75;

  //init page
  static const String welcome_page = 'WELCOME_PAGE';
  static const String calculator_page = 'CALCULATOR_PAGE';

  //Routes
  static const String home_page = '/';
  static const String main_menu_page = '/main_menu_page';
  static const String monitor_page = '/monitor_page';
  static const String calculator_collect_page = '/generate_collect_page';
  static const String collect_list_page = '/collect_list_page';
  static const String collect_page = '/collect_page';
  static const String register_page = '/register_page';

  //Constantes
  static const String bs_currency_abr  = 'Bs.';
  static const String sus_currency_abr  = 'Usd.';
}
