import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:mobpos/config.dart';
import 'package:mobpos/dto/data_user.dart';
import 'package:mobpos/pages/collect_page.dart';
import 'package:mobpos/pages/components/main_menu.dart';
import 'package:mobpos/pages/calculator_collect_page.dart';
import 'package:mobpos/pages/components/welcome_page2.dart';
import 'package:mobpos/pages/monitor_page.dart';
import 'package:mobpos/pocket_localizations.dart';
import 'package:mobpos/scoped_models/mainModel.dart';
import 'package:mobpos/themes/custom_theme.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:scoped_model/scoped_model.dart';


void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  String initPage = await DataUser.initPage();
  runApp(new Pos(initPage));
}


class Pos extends StatefulWidget {
  final String _initPage;
  
  Pos(this._initPage);
  @override
  State<StatefulWidget> createState() {
    return _PosState(_initPage);
  }
}

class _PosState extends State<Pos> {
  String _initPage;

  _PosState(this._initPage);

  final _platformChannel = MethodChannel('cirrus/deepUrl');

  Future<Null> _getDataDeepUrl() async{
    String dataUrl;
    try{
      final String result = await _platformChannel.invokeMethod('getDataDeepUrl');
      dataUrl = 'Data url is $result';
    }catch(error){
      dataUrl = 'Failed to get data url';
      print('error:: $error');
    }
    print(dataUrl);
  }

  @override
  void initState() {
    _getDataDeepUrl();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final MainModel model = MainModel();
    model.initPage = this._initPage;
    print('initpage model:: ${model.initPage}');
    var materialApp = MaterialApp(
      theme: CustomTheme.getThemeData(context).getMainTheme(),
      localizationsDelegates: [
        const PocketLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', 'US'), // English
        const Locale('es', 'ES'), // Español
        //// ... other locales the app supports
      ],
      routes:<String, WidgetBuilder>  {
      //  Config.root: (BuildContext context) => PinPage(),
        // ignore: missing_return
        Config.home_page: (BuildContext context) {
          switch (model.initPage){
            case Config.welcome_page : return WelcomePage2();
            case Config.calculator_page : return MainMenu();
          }
        },
        Config.welcome_page: (BuildContext context) => WelcomePage2(),
        Config.main_menu_page: (BuildContext context) => MainMenu(),
        Config.monitor_page: (BuildContext context) => MonitorPage(),
        Config.calculator_collect_page: (BuildContext context) => CalculatorCollectPage(),
      //  Config.collect_list_page: (BuildContext context) => CollectList(),
        Config.collect_page: (BuildContext context) => CollectPage(),
//        Config.register_page: (BuildContext context) => RegisterPage(),
      },
    );

    return ScopedModel<MainModel>(
        model: model,
        child: materialApp
      //materialApp,
    );
  }
}
