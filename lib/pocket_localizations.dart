import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import './l10n/messages_all.dart';
import 'package:simple_logger/simple_logger.dart';

class PocketLocalizations {

  static Future<PocketLocalizations> load(Locale locale) {
    final logger = SimpleLogger()..mode = LoggerMode.print;
    final String name = locale.countryCode.isEmpty ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);
    logger.info("PocketLocalizations name: $name localeName: $localeName ");
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      return PocketLocalizations();
    });
  }

  static PocketLocalizations of(BuildContext context) {
    return Localizations.of<PocketLocalizations>(context, PocketLocalizations);
  }
  String get enterCodeSms {
    return Intl.message(
      'Ingresa el codigo sms',
      name: 'enterCodeSms',
      desc: 'Etiqueta "Ingresa el codigo sms".',
    );
  }
  String get pos {
    return Intl.message(
      'POS',
      name: 'pos',
      desc: 'Etiqueta "POS".',
    );
  }
  String get movements {
    return Intl.message(
      'Movimientos',
      name: 'movements',
      desc: 'Etiqueta "Movimeintos".',
    );
  }
  String get unlinkDevice {
    return Intl.message(
      'Desvincular\ndispositivo',
      name: 'unlinkDevice',
      desc: 'Etiqueta "Desvincular dispositivo".',
    );
  }
  String get reports {
    return Intl.message(
      'Reportes',
      name: 'reports',
      desc: 'Etiqueta "Reportes".',
    );
  }
  String get paymentService {
    return Intl.message(
      'Pago de servicios',
      name: 'paymentService',
      desc: 'Etiqueta "Pago de servicios".',
    );
  }
  String get amountIsRequiredEmptyField{
    return Intl.message(
      'Importe es requerido.',
      name: 'amountIsRequiredEmptyField',
      desc: 'Etiqueta "Importe es requerido.".',
    );
  }
  String get amountMustBeLeast1{
    return Intl.message(
      'El importe debe ser 1.00 como mínimo',
      name: 'amountMustBeLeast1',
      desc: 'Etiqueta "El importe debe ser 1.00 como mínimo" para la pantalla transferencias',
    );
  }
  String get loadingYourAccounts{
    return Intl.message(
      'Cargando tus cuentas por favor espera...',
      name: 'loadingYourAccounts',
      desc: 'Etiqueta "Cargando tus cuentas por favor espere..." para la pantalla histórico',
    );
  }
  String get selectOriginAccount{
    return Intl.message(
      'Selecciona cuenta origen',
      name: 'selectOriginAccount',
      desc: 'Etiqueta "Selecciona cuenta origen" para la pantalla transferencias',
    );
  }
  String get accountThePayment{
    return Intl.message(
      'Cuenta de abono',
      name: 'accountThePayment',
      desc: 'Etiqueta "Cuenta de abono" para cancelar cobro Simple',
    );
  }
  String get selectAccountPayment{
    return Intl.message(
      'Selecciona cuenta de abono',
      name: 'selectAccountPayment',
      desc: 'Etiqueta "Seleccione cuenta de abono" para cancelar cobro Simple',
    );
  }
  String get notHaveAccountsFunds{
    return Intl.message(
        'No tienes cuentas habilitadas para hacer retiro de fondos',
        name: 'notHaveAccountsFunds',
        desc: 'Etiqueta "No cuenta con cuentas para hacer retiro de fondos" mensaje de error para preguntas de seguridad'
    );
  }
  String get accept{
    return Intl.message(
        'Aceptar',
        name: 'accept',
        desc: 'Etiqueta "Aceptar".'
    );
  }
  String get holder{
    return Intl.message(
        'Titular',
        name: 'holder',
        desc: 'Etiqueta "titular".'
    );
  }
  String get youMustEnterYourHolder {
    return Intl.message(
      'Ingresa nombre del titular',
      name: 'youMustEnterYourHolder',
      desc: 'Etiqueta "Ingresa nombre del titular".',
    );
  }
  String get company{
    return Intl.message(
        'Empresa',
        name: 'company',
        desc: 'Etiqueta "Empresa".'
    );
  }
  String get youMustEnterYourCompany {
    return Intl.message(
      'Ingresa el nombre de la empresa',
      name: 'youMustEnterYourCompany',
      desc: 'Etiqueta "Ingresa el nombre de la empresa".',
    );
  }
  String get mobileNumber{
    return Intl.message(
        'Número de celular',
        name: 'mobileNumber',
        desc: 'Etiqueta "Número de celular".'
    );
  }
  String get youMustEnterYourMobileNumber{
    return Intl.message(
      'Ingresa tu número de celular',
      name: 'youMustEnterYourMobileNumber',
      desc: 'Etiqueta "Ingresa tu número de celular".',
    );
  }
  String get loadingYourTransactions {
    return Intl.message(
      'Consultando sus transacciones, por favor espere...',
      name: 'loadingYourTransactions',
      desc: 'Etiqueta "Consultando sus transacciones, por favor espere...".',
    );
  }
  String get hasNoExtractAvailable{
    return Intl.message(
      'No tienes extracto disponible.',
      name: 'hasNoExtractAvailable',
      desc: 'Etiqueta "No tienes extracto disponible." .',
    );
  }
  String get theSessionHasExpired{
    return Intl.message(
        'La sesión ha expirado.',
        name: 'theSessionHasExpired',
        desc: 'Etiqueta "La sesión ha expirado. "'
    );
  }
  String get problemsWithTheService{
    return Intl.message(
        'Problemas con el servicio ',
        name: 'problemsWithTheService',
        desc: 'Etiqueta "Problemas con el servicio "'
    );
  }
  String get errorTryingToRenewTheSession{
    return Intl.message(
        'Error al intentar renovar la sesión.',
        name: 'errorTryingToRenewTheSession',
        desc: 'Etiqueta "Error al intentar renovar la sesión." cuando el token caduca y no se puede renovar el mismo por un nuevo periodo '
    );
  }
  String get appCanNotConnectToTheBankCheckYourConnection{
    return Intl.message(
        'La aplicación no puede conectar con el banco. Por favor revisa tu conexión a Internet.',
        name: 'appCanNotConnectToTheBankCheckYourConnection',
        desc: 'Etiqueta "La aplicación no puede conectarse con el banco. Por favor revisa tu conexión a Internet." mensaje de exception error'
    );
  }
  String get communicationBankLost{
    return Intl.message(
        'Se ha perdido comunicación con el banco.',
        name: 'communicationBankLost',
        desc: 'Etiqueta "Se ha perdido comunicación con el Banco."'
    );
  }
  String get tryAgainLater{
    return Intl.message(
        'Vuelve a intentarlo mas tarde.',
        name: 'tryAgainLater',
        desc: 'Etiqueta "Vuelve a intentarlo mas tarde."'
    );
  }
  String get hadProblemPleaseTryLater {
    return Intl.message(
        'Ocurrió un problema por favor inténtalo mas tarde.',
        name: 'hadProblemPleaseTryLater',
        desc: 'Etiqueta "Ocurrió un problema por favor inténtalo mas tarde." mensaje de exception error'
    );
  }
  String get checkInternetConnection{
    return Intl.message(
        'Por favor, revisa tu conexión a Internet.',
        name: 'checkInternetConnection',
        desc: 'Etiqueta "Por favor, revisa tu conexión a Internet."'
    );
  }
  String get canNotUnlink {
    return Intl.message(
        'No se pudo desvincular el dispositivo',
        name: 'canNotUnlink',
        desc: 'Mensaje "No se pudo desvincular el dispositivo" mensaje de error para desvincular dispositivo'
    );
  }
  String get canNotBeObtained {
    return Intl.message(
        'No se pudo obtener',
        name: 'canNotBeObtained',
        desc: 'Etiqueta "No se puede obtener" mensaje de error para preguntas de exception error'
    );
  }
  String get theHistoricalMovement{
    return Intl.message(
        'el histórico de movimientos.',
        name: 'theHistoricalMovement',
        desc: 'Etiqueta sufijo "el histórico de movimientos.", precedida por "No es posible cargar" '
    );
  }
  String get couldNotReadTheQrImagePleaseTryAgain{
    return Intl.message(
        'No se pudo leer la imagen QR, por favor inténtalo nuevamente.',
        name: 'couldNotReadTheQrImagePleaseTryAgain',
        desc: 'Etiqueta "No se pudo leer la imagen QR, por favor intenta nuevamente" '
    );
  }
  String get couldNotEncryptTheQrPleaseTryAgain{
    return Intl.message(
        'No se pudo encriptar el QR, por favor inténtalo nuevamente.',
        name: 'couldNotEncryptTheQrPleaseTryAgain',
        desc: 'Etiqueta "No se pudo encriptar el QR, por favor inténtalo nuevamente" '
    );
  }
  String get failedProcessingTransfer{
    return Intl.message(
        'Error al procesar la transferencia.',
        name: 'failedProcessingTransfer',
        desc: 'Etiqueta "Error al procesar la transferencia." mensaje de error para confirmación de transferencias'
    );
  }
  String get transferError{
    return Intl.message(
        'Error al intentar realizar transferencia,',
        name: 'transferError',
        desc: 'Error al intentar realizar la transferencia,"'
    );
  }
  String get theReportCouldNotBeObtainedPleaseTryItLater{
    return Intl.message(
        'No se pudo obtener el reporte, por favor inténtalo mas tarde.',
        name: 'theReportCouldNotBeObtainedPleaseTryItLater',
        desc: 'Etiqueta "No se pudo obtener el reporte, por favor inténtalo mas tarde" '
    );
  }
  String get simpleCollectError{
    return Intl.message(
      'Problemas con el cobro Simple',
      name: 'simpleChargeError',
      desc: 'Etiqueta "Error de cobro Simple" de Simple model',
    );
  }
  String get simpleValidateCollectError{
    return Intl.message(
      'Problemas al consultar el pago',
      name: 'simpleValidateCollectError',
      desc: 'Etiqueta "Problemas al consultar el pago" de Simple model',
    );
  }
  String get theValidateCouldNotBeObtainedPleaseTryItLater{
    return Intl.message(
        'No se pudo obtener la validación del pago, por favor inténtalo mas tarde.',
        name: 'theValidateCouldNotBeObtainedPleaseTryItLater',
        desc: 'Etiqueta "No se pudo obtener el reporte, por favor inténtalo mas tarde" '
    );
  }
  String get download{
    return Intl.message(
        'Descargar reporte',
        name: 'download',
        desc: 'Etiqueta "Descargar reporte" '
    );
  }
}

class PocketLocalizationsDelegate extends LocalizationsDelegate<PocketLocalizations> {
  const PocketLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'es'].contains(locale.languageCode);

  @override
  Future<PocketLocalizations> load(Locale locale) => PocketLocalizations.load(locale);

  @override
  bool shouldReload(PocketLocalizationsDelegate old) => false;
}