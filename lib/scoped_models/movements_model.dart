import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:mobpos/config.dart';
import 'package:mobpos/dto/product_dto.dart';
import 'package:mobpos/dto/response_moviment.dart';
import 'package:mobpos/helpers/dialog_util.dart';
import 'package:mobpos/helpers/exception_handler.dart';
import 'package:mobpos/helpers/http_jwt.dart';
import 'package:mobpos/pocket_localizations.dart';
import 'package:mobpos/scoped_models/connection_models.dart';
import 'package:url_launcher/url_launcher.dart';
import 'mainModel.dart';


mixin MovementModel on ConnectionModel {
  List<ProductDto> movements = List();
  List<ResponseMovement> extractMovements;
  final DatabaseReference databaseReference =
      FirebaseDatabase.instance.reference().child("pagos");

  //qr por defecto por el momento
  String _image = 'assets/qr_default.jpeg';

  //Reporte uuid
  String _uuid;
  String _urlReportMovements;

  get getUuidSimple => _uuid;

  get image => _image;

  set image(String value){
    this._image = value;
    notifyListeners();
  }

  void extractMovement(BuildContext context, String size, String date){
    extractMovements = null;
    movementsBl.getExtractMovements(context, size, date, (List<ResponseMovement> movement){
      extractMovements = movement;
      notifyListeners();
    }, (Exception ex){
      hideProgress();
      if(ex is PosServiceException){
        DialogUtil.showAcceptDialog(context, "Problemas al obtener movimientos", [Text(ex.response.errorDetail), Text(ex.response.statusCode)], PocketLocalizations.of(context).accept);
      }else{
        handleException(simple_limit, context, ex);
      }
    });
  }

  void uuidMovements(BuildContext context, String size, String date){
    _uuid = null;
    reportUuidBl.getSimpleUuid(context, size, date,
            (String uuid){
          _uuid = uuid;
          logger.info("uuid model: $_uuid");
          notifyListeners();
          hideProgress();
        }, (Exception ex) {
          hideProgress();
          if (ex is PosServiceException) {
            DialogUtil.showAcceptDialog(context, "Problemas al obtener uuid",
                [Text(ex.response.errorDetail), Text(ex.response.statusCode)],
                PocketLocalizations
                    .of(context)
                    .accept);
          } else {
            handleException(simple_movement, context, ex);
          }
        });
  }

  void reportMovements(BuildContext context, String uuid){
    showProgress();
    reportBl.getSimpleReport(
        context,
        uuid,
            (String urlReport){
          _urlReportMovements = urlReport;
          _launchUrlReport(_urlReportMovements);
          hideProgress();
        },
            (Exception ex){
          hideProgress();
          if(ex is PosServiceException){
            DialogUtil.showAcceptDialogWithFunction(context,
                'Problemas al obtener el reporte',
                [Text(ex.response.errorDetail), Text(ex.response.statusCode)],
                    (){
                  Navigator.pushReplacementNamed(context, Config.main_menu_page);
                }, PocketLocalizations.of(context).accept);
          }else{
            handleException(simple_movement, context, ex);
          }
        });
  }

  _launchUrlReport(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    }
  }

  void movementList() {
    databaseReference.once().then((DataSnapshot dataSnapshot) {
      for (var value in dataSnapshot.value.values) {
        movements.add(ProductDto.fromJson(value));
      }
      notifyListeners();
    });
  }

  void updatePayment(ProductDto productDto) {
    MainModel mainModel = MainModel();
    if (productDto != null) {
      databaseReference.child(productDto.idTransfer).set(productDto.toJson());
      notifyListeners();
      mainModel.isPayment = true;
    }
  }

  void searchItem(String id) {
    databaseReference
        .orderByChild('idTransfer')
        .equalTo(id)
        .once()
        .then((DataSnapshot dataSnapshot) {
      var childSnap;
      for (var value in dataSnapshot.value.values) {
        childSnap = value;
      }
      var productDto = ProductDto.fromJson(childSnap);
      print('encontrado $productDto');
      productDto = ProductDto(
          amount: productDto.amount,
          currency: productDto.currency,
          date: productDto.date,
          destination: productDto.destination,
          account: productDto.account,
          idTransfer: id,
          isPay: true);
      updatePayment(productDto);
    });
    databaseReference.child(id).update({});
  }
}
