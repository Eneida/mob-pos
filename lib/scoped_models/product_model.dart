import 'package:mobpos/dto/product_dto.dart';
import 'package:mobpos/scoped_models/connection_models.dart';

mixin ProductModel on ConnectionModel {
  List<ProductDto> _productList = List();
  String _totalAmount = '0.00';

  get totalAmount => _totalAmount;

  set totalAmount(String value) {
    this._totalAmount = value;
    notifyListeners();
  }

  get productList => _productList;

  void addProduct(ProductDto value) {
    print('adicionando $value');
    this._productList.add(value);
    print('lista... $_productList');
    notifyListeners();
  }

  set productList(List<ProductDto> valueList) {
    this._productList = valueList;
    notifyListeners();
  }
}
