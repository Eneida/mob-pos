import 'package:firebase_database/firebase_database.dart';
import 'package:mobpos/dto/product_dto.dart';
import 'package:mobpos/dto/response_monitor_simple.dart';
import 'package:mobpos/scoped_models/connection_models.dart';

mixin MonitorModel on ConnectionModel{

  List<ResponseMonitorSimple> monitorList;

  final String simpleMonitorUrl = 'https://ada-pos-4f42c.firebaseio.com/pagos.json';

  final DatabaseReference databaseReference = FirebaseDatabase.instance.reference().child("pagos");


  void simpleCollectAddFireBase(ProductDto product){
    print('Datos enviados::: $product');
//    var jsonTransfer = jsonEncode({"idTransfer": idTransfer, 'amount': amount, 'currency': currency, 'description': description, 'destination': destination, 'date': date, 'isPay': isPay});
//    http.post(simpleMonitorUrl, body: jsonTransfer);

    databaseReference.child(product.idTransfer).set(<dynamic, dynamic>{
      'idTransfer': product.idTransfer,
      'amount': product.amount,
      'currency': product.currency,
      'destination': product.destination,
      'account': product.account,
      'date': product.date,
      'isPay': product.isPay
    });
  }
}