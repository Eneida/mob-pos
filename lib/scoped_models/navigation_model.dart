import 'package:flutter/material.dart';
import '../scoped_models/connection_models.dart';

mixin NavigationModel on ConnectionModel {
  int _page = 0;
  PageController _nmPageController;

  get nmPage => _page;

  set nmPage(int page) {
    this._page = page;
    notifyListeners();
  }

  initNavigationModel(PageController pageController) {
    _nmPageController = pageController;
  }

  nmChangePage(int index){
    _page = index;
    _nmPageController?.animateToPage(_page??0,
                duration: const Duration(milliseconds: 300),
                curve: Curves.easeInOut);
    notifyListeners();
  }

  void destroyNavigationModel(){
    _page = 0;
    _nmPageController = null;
  }
}
