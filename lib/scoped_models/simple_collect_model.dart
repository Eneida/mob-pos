import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:http/http.dart';
import 'package:mobpos/config.dart';
import 'package:mobpos/dto/response_qr_collect.dart';
import 'package:mobpos/helpers/dialog_util.dart';
import 'package:mobpos/helpers/exception_handler.dart';
import 'package:mobpos/pocket_localizations.dart';
import 'package:mobpos/scoped_models/connection_models.dart';
import 'package:mobpos/scoped_models/mainModel.dart';
import 'package:toast/toast.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:path_provider/path_provider.dart';
import 'package:screen/screen.dart';


mixin SimpleCollectModel on ConnectionModel {
  String _urlImageSimple;

  ResponseQrCollect _dataQrEncrypt;
  int _originAccountCurrency;
  bool simple = false;
  String _stateCollect;
  bool _isPayment = false;
  int _idLastTransfer = 0;

  //Cambia el valor del tipo de cambio de acuerdo al importe
  MoneyMaskedTextController amountController = new MoneyMaskedTextController(
      decimalSeparator: '.', thousandSeparator: ',');

  var _opacity = 1.0;
  var _opacity2 = 0.0;

  AudioCache audioCache = AudioCache();
  AudioPlayer audioPlayer = AudioPlayer();
  String localFilePath;
  //Dirección audio de caja registradora
  String _urlAudio = 'http://thebeautybrains.com/wp-content/uploads/podcast/soundfx/cashregister.wav';

  get isPayment => _isPayment;

  set isPayment(bool value) {
    MainModel mainModel = (this as MainModel);
    this._isPayment = value;
    notifyListeners();
    if(_isPayment){
      _opacity = 0.0;
      _opacity2 = 1.0;
      //Reproduce el audio cuando el estado del cobro cambia a pagado
      mainModel.setUrlImageSimple = 'assets/gracias.png';
//      playNotification();
      playAudioCash();
    }
  }

  get isOpacity => _opacity;
  get isOpacity2 => _opacity2;

  set isOpacity(double value) {
    this._opacity = value;
    notifyListeners();

  }

  get idLastTransfer => _idLastTransfer;

  set generateId(int value) {
    this._idLastTransfer = value;
    notifyListeners();
  }

  ResponseQrCollect get dataQrEncrypt => _dataQrEncrypt;

  bool get getSimple => simple;
  set setSimple(bool value) {
    this.simple = value;
    notifyListeners();
  }

  get getOriginAccountCurrencySelected => _originAccountCurrency;

  set setOriginAccountCurrencySelected(int currency) {
    this._originAccountCurrency = currency;
    notifyListeners();
  }

  //url de la imagen generada en cobrar
  String get getUrlImageSimple => _urlImageSimple;    //para test
  get date => null;

  set setUrlImageSimple(String url) {
    this._urlImageSimple = url;
    notifyListeners();
  }

  //Guarda el audio localmente
  Future loadFileSound() async {
    final bytes = await readBytes(_urlAudio);
    final dir = await getApplicationDocumentsDirectory();
    final file = File('${dir.path}/audio.mp3');
    await file.writeAsBytes(bytes);
    if (await file.exists()) {
      localFilePath = file.path;
    }
  }

  //Se reproduce el audio desde una url
  Future<int> playNotification() async {
    int result = -1;
    if(localFilePath != null){
      result = await audioPlayer.play(localFilePath, isLocal: true);
      if (result == 1) {
        // success
        print('Success');
      }
    }else{
      result = await audioPlayer.play(_urlAudio);
    }
    return result;
  }

  //Se reproduce el audio desde locas cache
  void playAudioCash() {
    audioCache.play('cash_2.mp3');
  }

  // función para cobrar simple
  void qrCollect(
      BuildContext context,
      String account,
      int currency,
      String amount,
      String description,
      String expireDate,
      int singleUse,
      ) {
    _dataQrEncrypt = null;
    qrCollectBl.getQrCollect(
        context,
        account,
        currency,
        amount,
        description,
        expireDate,
        singleUse, (ResponseQrCollect responseQrCollect) {
      _dataQrEncrypt = responseQrCollect;

      print('qr encriptado :::: $_dataQrEncrypt');

      _urlImageSimple = "http://bfocore.cirrus-it.net:28282/ms-simple/v1/collect/image/${_dataQrEncrypt.qrId}";

      print("simppleImage: $_urlImageSimple ");
      hideProgress();
      notifyListeners();
      //Si generar el Qr es exitoso revien va a la pagina del detalle del qr generado
      Navigator.pushReplacementNamed(context, Config.collect_page);
    }, (Exception ex) {
      hideProgress();
      print('Error ::: $ex');
      if (ex is PosServiceException) {
        DialogUtil.showAcceptDialog(
            context,
            PocketLocalizations.of(context).simpleCollectError,
            [Text(ex.response.errorDetail), Text(ex.response.statusCode)],
            PocketLocalizations.of(context).accept);
      } else {
        handleException(simple_collect_exception, context, ex);
      }
    });
  }

  void validateStateCollect(BuildContext context,
      int idQr) {
    validateStateCollectBl.getValidateStateCollect(
        context, idQr,
            (String responseQrCollect) {
          MainModel mainModel = (this as MainModel);
          _stateCollect = responseQrCollect;
          if (_stateCollect.contains('El qr fue pagado.')) {
            mainModel.isPayment = true;
            Toast.show('El pago se efectuo exitosamente', context,
                duration: Toast.LENGTH_LONG);
            Screen.keepOn(false);
          } else {
            Toast.show(
                'El pago todavia no se efectuo, espere un momento por favor',
                context, duration: Toast.LENGTH_LONG);
          }
        },
            (Exception ex) {
          print('Error obteniendo el estado del cobro: $ex');
          if (ex is PosServiceException) {
            DialogUtil.showAcceptDialog(
                context,
                PocketLocalizations
                    .of(context)
                    .simpleValidateCollectError,
                [Text(ex.response.errorDetail), Text(ex.response.statusCode)],
                PocketLocalizations
                    .of(context)
                    .accept);
          } else {
            handleException(simple_collect_exception, context, ex);
          }
        });
  }

  void destroyPayment(){
    _idLastTransfer = 0;
    isPayment = false;
    _urlImageSimple = null;
    Screen.keepOn(false);
  }

}
