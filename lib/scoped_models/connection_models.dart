import 'package:mobpos/bl/movements_bl.dart';
import 'package:mobpos/bl/qr_collect_bl.dart';
import 'package:mobpos/bl/report_bl.dart';
import 'package:mobpos/bl/report_uuid_bl.dart';
import 'package:mobpos/bl/validate_state_collect_bl.dart';
import 'package:scoped_model/scoped_model.dart';

mixin ConnectionModel on Model{

  QrCollectBl qrCollectBl = QrCollectBl();
  ValidateStateCollectBl validateStateCollectBl = ValidateStateCollectBl();
  MovementsBl movementsBl = MovementsBl();
  ReportUuidBl reportUuidBl = ReportUuidBl();
  ReportBl reportBl = ReportBl();

double _deviceWidth = 0.0;

  get deviceWidth => _deviceWidth;
  set deviceWidth(double value){
    this._deviceWidth = value;
    notifyListeners();
  }

  double _deviceHeight = 0.0;

  get deviceHeight => _deviceHeight;
  set deviceHeight(double value){
    this._deviceHeight = value;
    notifyListeners();
  }

  bool _showProgress = false;

  bool get progress => _showProgress;

  void showProgress() {
    _showProgress = true;
    notifyListeners();
  }

  void hideProgress() {
    _showProgress = false;
    notifyListeners();
  }
}