import 'package:mobpos/scoped_models/product_model.dart';
import 'package:mobpos/scoped_models/register_model.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter/material.dart';
import 'package:mobpos/scoped_models/connection_models.dart';
import 'package:mobpos/scoped_models/monitor_model.dart';
import 'package:mobpos/scoped_models/navigation_model.dart';
import 'package:mobpos/scoped_models/simple_collect_model.dart';
import 'package:mobpos/scoped_models/movements_model.dart';
import 'package:mobpos/scoped_models/welcome_model.dart';

class MainModel extends Model with
    ConnectionModel,
    MonitorModel,
    NavigationModel,
    ProductModel,
    SimpleCollectModel,
    MovementModel,
    RegisterModel,
    WelcomeModel
{
  static MainModel of(BuildContext context) => ScopedModel.of<MainModel>(context);
}