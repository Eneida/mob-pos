import 'package:flutter_swiper/flutter_swiper.dart';
import '../scoped_models/connection_models.dart';

mixin WelcomeModel on ConnectionModel{

  SwiperController swiperController = SwiperController();
  bool _skip = false;

  String initPage;

  get getSkip => _skip;

  get getSwiperController => swiperController;

  set setSkip(bool value){
    this._skip = value;
    notifyListeners();
  }

  void destroyWelcome(){
    _skip = false;
  }
}