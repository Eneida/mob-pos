
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mobpos/bl/register_pos_bl.dart';
import 'package:mobpos/dto/data_user.dart';
import 'package:mobpos/dto/register_pos_response_dto.dart';
import 'package:mobpos/helpers/secure_storage.dart';
import 'package:mobpos/scoped_models/connection_models.dart';
import 'package:mobpos/scoped_models/mainModel.dart';

import '../config.dart';

mixin RegisterModel on ConnectionModel {

  String _tokenQr;
  String _registerPos;

  DataUser _dataUser;

  DataUser get dataUser => _dataUser;

  set dataUser(DataUser value){
    this._dataUser = value;
    notifyListeners();
  }

  String get tokenQr => _tokenQr;

  set tokenQr(String qr) {
    this._tokenQr = qr;
    notifyListeners();
  }

  String get registerPos => _registerPos;

  set registerPos(String value) {
    this._registerPos = value;
    notifyListeners();
  }


  void getRegisterPos(BuildContext context,
      Map<String, dynamic> formRegistration) {
    print('QR::: $tokenQr');
    // Autenticacion exitosa se guarda el Token JWT
    SecureStorage.write(Config.jwt_token_key, tokenQr);

    RegisterPosBl registerPosBl = RegisterPosBl();
    registerPosBl.registerPos(context, formRegistration['holderName'],
        formRegistration['companyName'], formRegistration['mobile'], (
            RegisterPosResponseDto result) {

        }, (Exception ex) {

        });
    Navigator.pushReplacementNamed(context, Config.main_menu_page);
  }

  DataUser decodeDataQr(String token){
    print('decodificando token::: $token');
    var list = token.split('.');
    print('lista::: $list');
    print('lista length::: ${list.length}');
    var codec = utf8.decode(base64.decode(base64Url.normalize(list[1])));
    print('codec::: $codec');
    Map userMap = jsonDecode(codec);
    dataUser = DataUser.fromJson(userMap);
    SecureStorage.write(Config.user_name, dataUser.name);
    SecureStorage.write(Config.user_account, dataUser.ac);
    SecureStorage.write(Config.user_ci, dataUser.di);
    print('dataUser::: $dataUser');
    return dataUser;
  }

  void saveDataUserModel() async {
    MainModel mainModel = (this as MainModel);
    print('leendo datos de secure');
    mainModel.tokenQr = await SecureStorage.read(Config.jwt_token_key);
    var name = await SecureStorage.read(Config.user_name);
    var account = await SecureStorage.read(Config.user_account);
    var ci = await SecureStorage.read(Config.user_ci);
    DataUser dataUser = DataUser(account, ci, name);
    mainModel.dataUser = dataUser;
    print('user guardado en el modelo ${mainModel.dataUser}');
  }
}
