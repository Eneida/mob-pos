import './pos_theme.dart';
import 'package:flutter/material.dart';

class AndroidTheme implements PosTheme {
  final BuildContext _context;

  const AndroidTheme(this._context);

  //Colores
  Color getPrimaryColor() {
    return Color(0xFFF9F9F9); //Blanco Gris
  }

  Color getAccentColor() {
    return Color(0xFF000069); //Azul
  }
  Color getPrimaryTextNegative() {
    return Color(0xFFF2F2F2);
  }
  @override
  double getAppBarElevation() {
    return 1.0;
  }

  @override
  Color getBackgroundForm() {
    return Color(0xFFF7FAFF);
  }

  @override
  ThemeData getBottomBarTheme() {
    var themeRequested = Theme.of(_context).copyWith(
      // sets the background color of the `BottomNavigationBar`
      canvasColor: getPrimaryColor(),
      // sets the active color of the `BottomNavigationBar` if `Brightness` is light// color icon
      primaryColor: getAccentColor(),
      textTheme: Theme.of(_context).textTheme.copyWith(
            caption: TextStyle(color: Color(0xFF747474)),
          ),
    );
    return themeRequested; //Theme.of(_context).copyWith();
  }

  @override
  Color getButtonColor() {
    return this.getPrimaryColor();
  }

  @override
  double getButtonRadius() {
    return 20.0;
  }

  @override
  Color getButtonTextColor() {
    return Colors.black;
  }

  @override
  Color getDividerColor() {
    return Color(0xFFB3B3B3);
  }

  @override
  Color getEmphasisButtonColor() {
    return this.getAccentColor();
  }

  @override
  Color getEmphasisButtonTextColor() {
    return Color(0xFFFFFFFF);
  }

  @override
  Color getErrorColor() {
    return Color(0xFFE37200);
  }

  @override
  Color getIconTheme() {
    return this.getPrimaryColor();
  }

  @override
  ThemeData getLoginTheme() {
    return ThemeData(
        errorColor: Colors.white,
        buttonColor: Color(0xFFE37200),
        textTheme: Theme.of(_context).textTheme.apply(
              bodyColor: Colors.black, // Usado para el inputtext
              displayColor: Colors.white, //Usado para el botton
            ));
  }

  @override
  ThemeData getMainTheme() {
    return ThemeData(
        brightness: Brightness.light,
        fontFamily: "Roboto",
        primaryColor: this.getPrimaryColor(),
        accentColor: this.getAccentColor(),
        dividerColor: this.getDividerColor(),
        primaryColorDark: this.getPrimaryColorDark(),
        primaryColorLight: this.getPrimaryColorLight(),
        buttonColor: this.getAccentColor(),
/*
        buttonTheme: ButtonThemeData(  //adicionar thema al boton
        ),
*/
        iconTheme:
            Theme.of(_context).iconTheme.copyWith(color: this.getIconTheme()),
        errorColor: this.getErrorColor(),
        textTheme: Theme.of(_context).textTheme.apply(
              bodyColor: this.getPrimaryText(), //primary text
              displayColor: this.getSecundaryText(), //secundary text
            ));
  }

  @override
  Color getPrimaryColorDark() {
    return Color(0xFFE8E9EB);
  }

  @override
  Color getPrimaryColorLight() {
    return Color(0xFFF1F8E9);
  }

  @override
  Color getPrimaryText() {
    return Color(0xFF000000);
  }

  @override
  Color getScaffoldBackground() {
    return Color(0xFFEFEFF4);
  }

  @override
  Color getSecundaryText() {
    return Color(0xFF020202);
  }

  Color getOffSwithColor() {
    return Color(0xFFD6D6D4);
    //return this.getPrimaryColor(); // Plomo
    //return Colors.blueGrey;
  }
}
