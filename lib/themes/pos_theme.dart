import 'package:flutter/material.dart';

abstract class PosTheme{
  //Colores
  Color getPrimaryColor();
  Color getAccentColor();
  Color getDividerColor();
  Color getPrimaryColorDark();
  Color getPrimaryColorLight();
  Color getPrimaryTextNegative();
  Color getIconTheme();
  Color getPrimaryText();
  Color getSecundaryText();
  Color getErrorColor();
  Color getScaffoldBackground();
  Color getBackgroundForm();

  //Tamaño del AppBar
  double getAppBarElevation();

  //Botones
  double getButtonRadius();
  Color getButtonTextColor();
  Color getButtonColor();
  Color getEmphasisButtonTextColor();
  Color getEmphasisButtonColor();

  //Themes
  ThemeData getLoginTheme();
  ThemeData getMainTheme();
  ThemeData getBottomBarTheme();

  Color getOffSwithColor();
}