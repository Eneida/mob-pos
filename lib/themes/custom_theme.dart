import 'package:flutter/material.dart';
import './pos_theme.dart';
import './android_theme.dart';
import './ios_theme.dart';

class CustomTheme{
  static PosTheme getThemeData(BuildContext context){
    if(Theme.of(context).platform == TargetPlatform.android){
      return AndroidTheme(context);
    } else {
      return IOsTheme(context);
    }
  }

  static isIOs(context){
    return Theme.of(context).platform == TargetPlatform.iOS;
  }
}