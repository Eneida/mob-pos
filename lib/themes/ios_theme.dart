import './pos_theme.dart';
import 'package:flutter/material.dart';

class IOsTheme implements PosTheme{
  final BuildContext _context;
  const IOsTheme(this._context);
  //Colores
  Color getPrimaryColor() {
    return Color(0xFFF9F9F9); //Gris blanco
  }

  Color getAccentColor() {
    return Color(0xFF000069); //Azul
  }

  Color getDividerColor() {
    return Color(0xFFB3B3B3);
  }

  Color getPrimaryColorDark() {
    return Color(0xFFE8E9EB);
  }

  Color getPrimaryColorLight() {
    return Color(0xFFC5CAE9);
  }

  Color getIconTheme() {
    return this.getPrimaryColor();
  }
  Color getPrimaryTextNegative() {
    return Color(0xFFF2F2F2);
  }
  Color getPrimaryText() {
    return Color(0xFF000000);
  }

  Color getSecundaryText() {
    return Color(0xFF020202);
  }

  Color getErrorColor() {
    return Color(0xFF000069);
  }

  Color getLoginButton() {
    return Color(0xFF000069);
  }

  Color getLoginText() {
    //Reemplazar con theme login
    return Colors.white;
  }

  Color getScaffoldBackground() {
    return Color(0xFFEFEFF4);
  }

  Color getButtonTextColor() {
    return Colors.black;
  }

  Color getButtonColor() {
    return this.getPrimaryColor(); //Gris claro
  }

  Color getEmphasisButtonTextColor() {
    return Color(0xFFFFFFFF); // Blanco
  }

  Color getEmphasisButtonColor() {
    return this.getAccentColor(); // Naranja
  }

  Color getOnSwithColor() {
    return this.getAccentColor(); // Naranja
  }
  Color getOnTextSwithColor() {
    return Colors.white; // Naranja
  }
  Color getOffSwithColor() {
    return Color(0xFFD6D6D4);
    //return this.getPrimaryColor(); // Plomo
    //return Colors.blueGrey;
  }
  Color getOffTextSwithColor() {
    return this.getDividerColor();
//    return Colors.black; //Negro
  }

  double getAppBarElevation() {
    return 1.0;
  }

  double getButtonRadius() {
    return 10.0;
  }

  Color getBackgroundForm() {
    return this.getPrimaryColor();
  }

  //Themes
  ThemeData getLoginTheme() {
    return ThemeData(
        errorColor: Colors.white,
        buttonColor: Color(0xFF000069),
        textTheme: Theme.of(_context).textTheme.apply(
          bodyColor: Colors.black, // Usado para el inputtext
          displayColor: Colors.white, //Usado para el botton
        ));
  }

  ThemeData getMainTheme() {
    return ThemeData(
        brightness: Brightness.light,
        fontFamily: "SF-Pro-Display",
        primaryColor: this.getPrimaryColor(),
        accentColor: this.getAccentColor(),
        dividerColor: this.getDividerColor(),
        primaryColorDark: this.getPrimaryColorDark(),
        primaryColorLight: this.getPrimaryColorLight(),
        buttonColor: this.getAccentColor(),
        iconTheme:
        Theme.of(_context).iconTheme.copyWith(color: this.getIconTheme()),
        errorColor: this.getErrorColor(),
        textTheme: Theme.of(_context).textTheme.apply(
          bodyColor: this.getPrimaryText(), //primary text
          displayColor: this.getSecundaryText(), //secundary text
        ));
  }

  ThemeData getBottomBarTheme() {
    var themeRequested = Theme.of(_context).copyWith(
      // sets the background color of the `BottomNavigationBar`
      canvasColor: getPrimaryColor(),
      // sets the active color of the `BottomNavigationBar` if `Brightness` is light// color icon
      primaryColor: getAccentColor(),
      textTheme: Theme.of(_context).textTheme.copyWith(
        caption: TextStyle(color: Color(0xFF747474)),
      ),
    );
    return themeRequested;//Theme.of(_context).copyWith();
  }

}
