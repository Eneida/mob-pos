import 'package:mobpos/service/report_service.dart';

import '../helpers/exception_handler.dart';
import 'package:flutter/material.dart';

class ReportBl{
  static final ReportBl _instance = new ReportBl._internal();

  factory ReportBl(){
    return _instance;
  }

  ReportBl._internal();

  void getSimpleReport(BuildContext context, String uuid, Function (String urlReport) success, Function error){
    ReportService reportService = ReportService();
    reportService.querySimpleReport(context, uuid).then((response){
      success(response);
    }).catchError((ex){
      error(processCatchError(ex));
    });
  }
}