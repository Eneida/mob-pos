import 'package:flutter/material.dart';
import 'package:mobpos/dto/pos_response.dart';
import 'package:mobpos/dto/response_moviment.dart';
import 'package:mobpos/helpers/exception_handler.dart';
import 'package:mobpos/service/movements_service.dart';

class MovementsBl {
  static final MovementsBl _instance = new MovementsBl._internal();

  factory MovementsBl() {
    return _instance;
  }

  MovementsBl._internal();

  void getExtractMovements(
      BuildContext context, String size, String date, Function success, Function error) {
    MovementsService movementsService = new MovementsService();
    movementsService.queryMovements(context, size, date).then((response) {
      var posResponse = PosResponse.fromJson(response.data);
      List<ResponseMovement> responseMovements = List();
      if (posResponse.status) {
        var movementsListResponse = posResponse.response;
        if(movementsListResponse.length >0){
          movementsListResponse.forEach((dynamic movementsJson) {
            responseMovements.add(ResponseMovement.fromJson(movementsJson));
          });
        }
        success(responseMovements);
      }else{
        error(posResponse.errorDetail);
      }
    }).catchError((ex){
      error(processCatchError(ex));
    });
  }
}
