import 'package:flutter/material.dart';
import 'package:mobpos/dto/pos_response.dart';
import 'package:mobpos/helpers/exception_handler.dart';
import 'package:mobpos/service/validate_state_collect_service.dart';

class ValidateStateCollectBl{
  static final ValidateStateCollectBl _instance = new ValidateStateCollectBl._internal();

  factory ValidateStateCollectBl() {
    return _instance;
  }

  ValidateStateCollectBl._internal();

  void getValidateStateCollect(BuildContext context,
      int idQr, Function(String responseQrCollect) success,
      Function error) {
    ValidateStateCollectService validateStateCollectService = ValidateStateCollectService();
    validateStateCollectService.queryValidateStateCollect(
        context,
        idQr,).then((response){
      PosResponse posResponse = PosResponse.fromJson(response.data);
      print('response validate::: ${posResponse.response}');
      if(posResponse.status){
        success(posResponse.response);
      }else{
        error(posResponse.errorDetail);
      }
    }).catchError((ex){
      error(processCatchError(ex));
    });
  }
}