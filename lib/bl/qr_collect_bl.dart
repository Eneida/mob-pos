import 'package:mobpos/dto/pos_response.dart';
import 'package:mobpos/dto/response_qr_collect.dart';
import 'package:mobpos/helpers/exception_handler.dart';
import 'package:mobpos/service/simple_qr_collect_service.dart';

import 'package:flutter/material.dart';

class QrCollectBl{
  static final QrCollectBl _instance = new QrCollectBl._internal();

  factory QrCollectBl() {
    return _instance;
  }

  QrCollectBl._internal();

  void getQrCollect(BuildContext context,
      String account, int currency, String amount, String description, String expireDate,
      int singleUse, Function(ResponseQrCollect responseQrCollect) success,
      Function error) {
    SimpleQrCollectService simpleQrCollectService = SimpleQrCollectService();
    simpleQrCollectService.queryCollectQr(
        context,
        account,
        currency,
        amount,
        description,
        expireDate,
        singleUse).then((response){

      PosResponse pocketResponse = PosResponse.fromJson(response.data);
      if(pocketResponse.status){
        ResponseQrCollect responseQrCollect = ResponseQrCollect(pocketResponse.response['qrId']);
        success(responseQrCollect);
      }else{
        error(pocketResponse.errorDetail);
      }
    })
        .catchError((ex){
      error(processCatchError(ex));
    });
  }
}