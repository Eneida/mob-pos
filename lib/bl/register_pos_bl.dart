import 'package:flutter/material.dart';
import 'package:mobpos/dto/pos_response.dart';
import 'package:mobpos/dto/register_pos_response_dto.dart';
import 'package:mobpos/service/register_pos_service.dart';

class RegisterPosBl {
  static final RegisterPosBl _instance =
  new RegisterPosBl._internal();

  factory RegisterPosBl() {
    return _instance;
  }

  RegisterPosBl._internal();

  void registerPos(
      BuildContext context,
      String holderName,
      String company,
      String mobileNumber,
      Function success(RegisterPosResponseDto result),
      Function(Exception) error
      ) async {
    RegisterPosService registerPosService = RegisterPosService();

    registerPosService.queryRegisterPos(context, holderName, company, mobileNumber).then((response){
      var posResponse = PosResponse.fromJson(response.data);
      if (posResponse.status) {

        RegisterPosResponseDto registerPosResponseDto =
        RegisterPosResponseDto.fromJson(posResponse.response);

        success(registerPosResponseDto);
      } else {
        error(Exception(posResponse.errorDetail));
      }
    }).catchError((ex){
      error(Exception(ex));
    });
  }
}