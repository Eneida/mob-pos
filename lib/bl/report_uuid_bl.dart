import 'package:flutter/material.dart';
import 'package:mobpos/dto/pos_response.dart';
import 'package:mobpos/service/report_uuid_service.dart';
import '../helpers/exception_handler.dart';

class ReportUuidBl{
  static final ReportUuidBl _instance = new ReportUuidBl._internal();

  factory ReportUuidBl(){
    return _instance;
  }
  ReportUuidBl._internal();

  void getSimpleUuid(BuildContext context,
      String size, String date,
      Function success,
      Function error){
    ReportUuidService simpleUuid = ReportUuidService();
    simpleUuid.queryReportUuid(context, size, date).then((response){
      var pocketResponse = PosResponse.fromJson(response.data);
      if(pocketResponse.status){
        success(pocketResponse.response);
      }else{
        error(Exception(pocketResponse.errorDetail));
      }
    }).catchError((ex){
      error(processCatchError(ex));
    });
  }
}