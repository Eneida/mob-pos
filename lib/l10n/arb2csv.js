var fs = require('fs');
var obj = JSON.parse(fs.readFileSync('intl_messages.arb', 'utf8'));
var property = Object.getOwnPropertyNames(obj);
fs.writeFileSync("output.csv", "Variable|Descripción|Español|Inglés\n", function(err) {
  if(err) {
    return console.log(err);
  }
});
for (var i = 0; i < property.length; i++ ) {
  var prop = property[i];
  if (!prop.startsWith('@')) {
//    console.log(obj[prop]);
//    console.log(obj['@'+prop].description);
    fs.appendFileSync("output.csv", prop+"|"+obj['@'+prop].description+'|'+obj[prop]+"||"+"\n", function(err) {
      if(err) {
        return console.log(err);
      }
    });
  }
}
