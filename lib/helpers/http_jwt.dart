import 'dart:async';
import 'package:flutter/material.dart';
import 'package:mobpos/dto/pos_response.dart';
import 'package:mobpos/helpers/exception_handler.dart';
import 'package:mobpos/pocket_localizations.dart';
import 'package:simple_logger/simple_logger.dart';
import '../helpers/secure_storage.dart';
import '../config.dart';
import '../scoped_models/mainModel.dart';
import 'package:dio/dio.dart';


String _refreshUrl = Config.url_base + '/ms-home-banking/v1/auth';
final logger = SimpleLogger()..mode = LoggerMode.print;

Future<Response> get(BuildContext context, url, {Map<String, String> headers}) async {
  Options options = await _prepareOptions(headers);
  var request = (_url, _options) => Dio().get(Uri.encodeFull(_url), options: _options);
  Response response = await invokeRequest(request, url, options);

  if(response.statusCode > 299) {
    logger.info("StatusCode ${response.statusCode} para $url. BODY: ${response.data}");
    if (response.statusCode == 401) {
      // El backend nos retorna junto con el 401 un header 'refresh-token' que indica si debemos intenter refrescar
      var refreshToken = response.headers.value("refresh-token");
      if (refreshToken != null && refreshToken == "true") {
        response = await _refreshTokenAndRetry(request, url, headers, Dio(), context);
      } else {
        logger.warning("La session ha expirado.");
        throw SessionExpiredException("${PocketLocalizations.of(context).theSessionHasExpired}");
      }
    } else {
      _processRequestError(response, url, context);
    }
  }

  return response;
}

Future<Response> post(BuildContext context, url, dynamic body, {Map<String, String> headers}) async {
  Options options = await _prepareOptions(headers);
  var request = (_url,_body,_options) => Dio().post(Uri.encodeFull(_url),  options: _options, data:_body);
  Response response = await invokeRequest(request, url, options, body: body);
  if(response.statusCode > 299) {
    logger.info("StatusCode ${response.statusCode} para $url. BODY: ${response.data}");
    if (response.statusCode == 401) {
      // El backend nos retorna junto con el 401 un header 'refresh-token' que indica si debemos intenter refrescar
      var refreshToken = response.headers.value("refresh-token");
      if (refreshToken != null && refreshToken == "true") {
        response = await _refreshTokenAndRetry(request, url, headers, Dio(), context);
      } else {
        logger.warning("La session ha expirado.");
        throw SessionExpiredException("${PocketLocalizations.of(context).theSessionHasExpired}");
      }
    } else {
      _processRequestError(response, url, context);
    }
  }
  return response;
}

Future<Options> _prepareOptions(Map<String, String> headers) async {
  String token = await SecureStorage.read(Config.jwt_token_key).then( (value) => value);
  if(headers == null) {
    headers = Map();
  }
  headers.addAll({"Authorization":"Bearer $token"});
  Options options = Options(headers: headers);
  return options;
}

Future<Response> invokeRequest(Function request, String url, Options options, {dynamic body}) async {
  Response response;
  try {
    if(body == null){
      response = await request(url, options);
    }else{
      response = await request(url, body, options);
    }
  } catch (e) {
    if (e is DioError && e.type == DioErrorType.RESPONSE) {
      response = e.response;
    } else {
      throw PosException(e.toString());
    }
  }
  return response;
}

Future<Response> _refreshTokenAndRetry( Function request, String url, Map<String, String> headers, Dio dio, BuildContext context) async {
  Response response;
  String token = await SecureStorage.read(Config.jwt_token_key).then( (value) => value);
  logger.info("Intentando refrescar token");
  Response responseRefresh;
  try {
    responseRefresh = await dio.put(Uri.encodeFull(_refreshUrl), data: {"token": token});
  } catch (e) { //Capturamos posible falsa excepción
    // El servicio ha retornado un mensaje diferente de code=200, pero para Dio  esto es un error
    if (e is DioError && e.type == DioErrorType.RESPONSE) {
      responseRefresh = e.response;
      String message = "Error al intentar refrescar token: ${e.response}";
      logger.info(message);
//      Crashlytics.instance.log(message);
    } else {
      String message = "Excepcion al intentar refrescar token: ${e.toString()}";
      logger.info(message);
//      Crashlytics.instance.log(message);
      throw PosException(e.toString());
    }
  }
  // Si la respuesta es satisdactoria.
  if (responseRefresh.statusCode == 200) {
    //guardamos el nuevo token
    String newToken = responseRefresh.headers.value("authorization");
    //Quitamos el prefijo "Bearer "
    String token = newToken.replaceAll("Bearer ","");
    //Guardamo en el segure storage para futuras invocaciones
    SecureStorage.write(Config.jwt_token_key, token);
    //Colocamos el token en el modelo
    MainModel.of(context).tokenQr = token;
    logger.info("Token refrescado con éxito");
    Options options = await _prepareOptions(headers);
    try {
      response = await request(url, options);
    } catch(e) {
      if (e is DioError && e.type == DioErrorType.RESPONSE) {
        response = e.response;
        String message = "Error al reintentar la solicitud luego de refrescar token URL: $url Response: ${e.response}";
        logger.info(message);
//        Crashlytics.instance.log(message);
      } else {
        String message = "Excepcion al reintentar la solicitud luego de refrescar token URL: $url Response: ${e.toStrint()}";
        logger.info(message);
//        Crashlytics.instance.log(message);
        throw PosException(e.toString());
      }
    }
  } else {
    throw SessionExpiredException("${PocketLocalizations.of(context).errorTryingToRenewTheSession} RESCOD:${response.statusCode}");
  }
  return response;
}

void _processRequestError(Response response, String url, BuildContext context) {
  String message = "Error al invocar al servicio 1:$url  -  status:${response.statusCode} ${response.statusCode.runtimeType}  -  \nheaders 1:${response.headers}\nbody 1:${response.data}";
//  Crashlytics.instance.log(message);
  logger.warning(message);
  var pocketResponse = PosResponse.fromJson(response.data);
  logger.info('Response status: ${pocketResponse.statusCode}');
  logger.info('Response response: ${pocketResponse.response}');
  logger.info('Response error detail : ${pocketResponse.errorDetail}');
  logger.warning('Response \nstatus: ${pocketResponse.statusCode} \nresponse: ${pocketResponse.response} \nerrorDetail ${pocketResponse.errorDetail}');
  if (response.statusCode < 500 && response.statusCode >= 300){
    throw PosServiceException(PosResponse.fromJson(response.data));
  } else {
    throw PosException("${PocketLocalizations.of(context).problemsWithTheService} ${pocketResponse.errorDetail != null? pocketResponse.errorDetail: ''}  ${pocketResponse.statusCode != null? pocketResponse.statusCode: response.statusCode}");
  }
}