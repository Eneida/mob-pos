import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:async';

class SecureStorage {


  static final _storage = new FlutterSecureStorage();

  static void write(String keyValue, String value) async {
    await _storage.write(key: keyValue, value: value);
  }

  static Future<String> read(String keyValue) async {
    String result = await _storage.read(key: keyValue);
    return (result);
  }

  static void delete(String keyValue) {
    _storage.delete(key: keyValue);
  }
}