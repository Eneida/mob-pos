import 'dart:io';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
import 'package:crypto/crypto.dart';

  Dio initDio() {
    Transformer transformer = new DefaultTransformer();
    Dio dio = Dio();
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (HttpClient client) {
      SecurityContext sc = new SecurityContext();
      var cert = utf8.encode(_getCertificate());
      sc.setTrustedCertificatesBytes(cert);
      HttpClient httpClient = new HttpClient(context: sc);
      httpClient.badCertificateCallback=(X509Certificate cert, String host, int port){
        return cert.pem == _getCertificate();
      };
      return httpClient;
    };
    dio.interceptors.add(InterceptorsWrapper(
      onRequest:(RequestOptions options) async {
      // Do something before request is sent
      var data  = options.data;
        //bytes = utf8.encode(_data);
        if (["POST", "PUT", "PATCH"].contains(options.method)) {
          if( !(data is FormData) && !(data is List<int>) ) {
            String dataEncoded = await transformer.transformRequest(options);
            String digest = calculateHmac(dataEncoded);
            options.headers['Content-MD5'] = digest;
          }
        }
        return options; //continue
        // If you want to resolve the request with some custom data，
        // you can return a `Response` object or return `dio.resolve(data)`.
        // If you want to reject the request with a error message, 
        // you can return a `DioError` object or return `dio.reject(errMsg)`    
        }
    ));
    return dio;
  } 

  String calculateHmac(String body) {
    var key = utf8.encode('ABCabc123');
    var bytes = utf8.encode(body);

    var hmacSha256 = new Hmac(sha256, key); // HMAC-SHA256
    var digest = hmacSha256.convert(bytes);
    return digest.toString();
  }

  String _getCertificate() {   
    return """-----BEGIN CERTIFICATE-----
MIIG+jCCBeKgAwIBAgIRAKNnUpfX5Nl4GQTeT8gAycgwDQYJKoZIhvcNAQELBQAw
gZAxCzAJBgNVBAYTAkdCMRswGQYDVQQIExJHcmVhdGVyIE1hbmNoZXN0ZXIxEDAO
BgNVBAcTB1NhbGZvcmQxGjAYBgNVBAoTEUNPTU9ETyBDQSBMaW1pdGVkMTYwNAYD
VQQDEy1DT01PRE8gUlNBIERvbWFpbiBWYWxpZGF0aW9uIFNlY3VyZSBTZXJ2ZXIg
Q0EwHhcNMTkwMTA5MDAwMDAwWhcNMjEwMTA4MjM1OTU5WjBfMSEwHwYDVQQLExhE
b21haW4gQ29udHJvbCBWYWxpZGF0ZWQxFDASBgNVBAsTC1Bvc2l0aXZlU1NMMSQw
IgYDVQQDExttb3ZpbC5iYW5jb2ZvcnRhbGV6YS5jb20uYm8wggEiMA0GCSqGSIb3
DQEBAQUAA4IBDwAwggEKAoIBAQDPjUOvnnn13z2/DzGRsKpX5ikugcZwWiVdetj4
5d52Xy7Dr5QJnlULaeGMUIcD6fDeJasjREt4NhSQ8YsSTXuHmDHPdxmR9jWQmFTc
Y8j/JHfJ9uUkuror6157pgy/gk20tpscorjHS5I2OtEDxza28PBaXyjbC/VAJ9BN
Shw/Qumv3067OlcLRtlbobN7Oa0TwkQ1sSYf99um3gtvqNQ4faAcmFuFheCsxT25
Vt5RfZred+6kduHOlnrAXElfR8mN8yOKriNJWhtEaAzSfEBCVll3xZuELWpjS9jw
ycDzkEBxI43TV6/Bd+24pCzORMZvzF6kFxfN1Oo45lYA9+qxAgMBAAGjggN9MIID
eTAfBgNVHSMEGDAWgBSQr2o6lFoL2JDqElZz30O0Oija5zAdBgNVHQ4EFgQUPWXv
S/62CDINI4vuok5BWOVZmmYwDgYDVR0PAQH/BAQDAgWgMAwGA1UdEwEB/wQCMAAw
HQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCME8GA1UdIARIMEYwOgYLKwYB
BAGyMQECAgcwKzApBggrBgEFBQcCARYdaHR0cHM6Ly9zZWN1cmUuY29tb2RvLmNv
bS9DUFMwCAYGZ4EMAQIBMFQGA1UdHwRNMEswSaBHoEWGQ2h0dHA6Ly9jcmwuY29t
b2RvY2EuY29tL0NPTU9ET1JTQURvbWFpblZhbGlkYXRpb25TZWN1cmVTZXJ2ZXJD
QS5jcmwwgYUGCCsGAQUFBwEBBHkwdzBPBggrBgEFBQcwAoZDaHR0cDovL2NydC5j
b21vZG9jYS5jb20vQ09NT0RPUlNBRG9tYWluVmFsaWRhdGlvblNlY3VyZVNlcnZl
ckNBLmNydDAkBggrBgEFBQcwAYYYaHR0cDovL29jc3AuY29tb2RvY2EuY29tMEcG
A1UdEQRAMD6CG21vdmlsLmJhbmNvZm9ydGFsZXphLmNvbS5ib4Ifd3d3Lm1vdmls
LmJhbmNvZm9ydGFsZXphLmNvbS5ibzCCAYAGCisGAQQB1nkCBAIEggFwBIIBbAFq
AHcAu9nfvB+KcbWTlCOXqpJ7RzhXlQqrUugakJZkNo4e0YUAAAFoNFzXqgAABAMA
SDBGAiEAwEr2h8lf9FwtI5x18cqEAHlhtdVeAXd0K/zIYhE65/kCIQDI5AQ+gtvg
/89hyDDIXl+opBav2dNpYdddAal4SXhgNgB3AESUZS6w7s6vxEAH2Kj+KMDa5oK+
2MsxtT/TM5a1toGoAAABaDRc1/gAAAQDAEgwRgIhALJwgJ1MB2NpfSQf3TQXx+NT
eG8+r++HTT7yrFiOT6n5AiEAt6B9VDDq4Vagrz2Ze4kfzruEmw6HKMGEOOt37G9I
vNUAdgBc3EOS/uarRUSxXprUVuYQN/vV+kfcoXOUsl7m9scOygAAAWg0XNfxAAAE
AwBHMEUCIQCs9lr8ztJBZha7+XOFb6rgZ2yZPndxH13zFKkymik5awIgMlvVpH15
egbIcPCv0vFhBQcWILgaXJykbB2RPAW0/SAwDQYJKoZIhvcNAQELBQADggEBAHuM
G2/HWiLVce7X1E8zHqLE2yqD+xgZcQpiyb+hwalMky/7FwGn2ZSWz6qeD9Pvj8YZ
+Y2eId7d/uazdRejTeNo/4xdOLcAgBePHrEPi1ATff4bx2+VApIPNYg1Z4UkxrVR
UdPXaG9B9KZLafv3JtpOLU/Ram1IHbVSamyHKpemjJVoIEjVyRzEft/JkYPjcyc9
cguFgd/dQZZl0ECfC2utNI805DBqUK9oFG1XiCoDwhXmHgtfDXQDCIayxymlfQ2h
LHB4gFaO3xvK35yRcwmlHzgKxRADkPaRA189X/bwpb3Xtnlbx0SKrEMa8Hlt6gLd
b57agfqewRn+J09LUKU=
-----END CERTIFICATE-----
""";
  }