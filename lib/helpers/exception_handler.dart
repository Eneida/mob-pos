import 'package:flutter/material.dart';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:mobpos/dto/pos_response.dart';
import 'package:simple_logger/simple_logger.dart';

import '../helpers/dialog_util.dart';
import '../scoped_models/mainModel.dart';
import '../pocket_localizations.dart';

//Aplicacion
const int balance_exception = 100;
const int balance_detail_exception = 101;
const int account_history_exception = 102;
const int save_beneficiary_exception = 103;
const int save_other_bank_exception =104;
const int load_banks_exception =105;
const int load_origin_accounts_exception =106;
const int load_target_accounts_exception =107;
const int make_transfer_exception =108;
const int confirm_transfer_exception =109;
const int confirm_third_transfer_exception =110;
const int third_account_validation_exception =111;
const int exchange_validation_exception =112;
const int delete_beneficiary_exception =113;
const int payment_credit_detail_exception =114;
const int authorizations_exception =115;
const int payment_credit_exception =116;
const int confirm_payment_credit_exception =117;
const int unlink_device_exception = 118;
const int no_internet_exception = 119;

const int simple_payment_exception = 120;
const int simple_collect_exception = 121;
const int simple_update_limit = 122;
const int simple_movement = 123;
const int simple_limit = 124;
const int simple_validate_collect_exception = 125;

//Seguridad
const int biometric_login_exception = 200;
const int normal_login_exception = 201;


void handleException(int code, BuildContext context, Object ex) {
  //Inicializand logger
  final logger = SimpleLogger()..mode = LoggerMode.print;

  logger.warning("HANDLE EXCEPTION: $ex");
  logger.warning("TYPE EXCEPTION: ${ex.runtimeType}");
  // Si la session ha expirado lo sacamos al usuario
  if(ex is SessionExpiredException) {
    logger.warning('SessionExpiredException: La sessión a expirado');
//    MainModel.of(context).sessionLogout(context, true);
    return;
  }
  String exceptionMessage;
  //So es una PocketException  disparada por el codigo Dart en la App Móvil
  if (ex != null && ex is PosException) {
    String message = 'PocketException: ${ex._message}';
    logger.info(message);
    exceptionMessage = ex.message;
  } else if (ex != null && ex is PosServiceException) {    //Erro al invocar al micro-servicio
    String message = 'PocketServiceException: ${ex.response}';
    logger.info(message);
    exceptionMessage = "${ex.response.errorDetail} - ${ex.response.statusCode}" ;
  } else if (ex is SocketException) {
    String message = 'SocketException: ${ex.message}';
    logger.info(message);
    exceptionMessage = PocketLocalizations.of(context).appCanNotConnectToTheBankCheckYourConnection;
    //Se asigna un codigo para personalizar el mensaje de error.
    code = no_internet_exception;
  } else if (ex is DioError && ex.type == DioErrorType.DEFAULT && (ex.message.contains("No address associated with hostname") || ex.message.contains("Failed host lookup") || ex.message.contains('Software caused connection abort'))) {
    String message = 'DioError type:${ex.type} message:${ex.message}';
    logger.info(message);
    exceptionMessage = PocketLocalizations.of(context).appCanNotConnectToTheBankCheckYourConnection;
    code = no_internet_exception;
  } else if (ex is NoSuchMethodError) {
    String message = 'NoSuchMethodError "No existe el método invocado ${ex.toString()}"';
    logger.info(message);
    //Mostrando error generico
    exceptionMessage = '${PocketLocalizations.of(context).communicationBankLost} ${PocketLocalizations.of(context).tryAgainLater}';
  } else {
    String message = 'SEVERE: handlerException: No se pudo determinar el error runtimeType:${ex.runtimeType} toString:${ex.toString()}';
    String stackTrace = StackTrace.current.toString();
    logger.info(message);
    logger.info(stackTrace);
    exceptionMessage = PocketLocalizations.of(context).hadProblemPleaseTryLater;
  }

  //  Parche para mejorar el mensaje de conexion a internete
  //TODO Determinar en cual bloque va este mensaje y internacionalizarl
  if(exceptionMessage.contains('No address associated with hostname')){
    logger.warning("Eneida cuando veas este mensaje te fijas por cual excepción debería colocarse este mensaje");
    exceptionMessage = "Por favor, revise su conexión a Internet, e inténtalo nuevamente";
  }

  logger.warning("Handling exception code: $code");
  switch(code) {
    case no_internet_exception: // Problema al obtener el balance de cuentas.
      _showErrorMessage(context, 
        code, 
        "${PocketLocalizations.of(context).checkInternetConnection}",
        [exceptionMessage]);
        break;
    case unlink_device_exception: // Problema al obtener el balance de cuentas.
      _showErrorMessage(context, 
        code, 
        "${PocketLocalizations.of(context).canNotUnlink}",
        [exceptionMessage]);
        break;
    case account_history_exception: // Problema al obtener el histórico de movimientos entre cuentas
      _showErrorMessage(context, 
        code, 
        "${PocketLocalizations.of(context).canNotBeObtained} ${PocketLocalizations.of(context).theHistoricalMovement}",
        [exceptionMessage]);
        break;
    case make_transfer_exception: // Problema al cargar cuentas destino
      _showErrorMessage(context, 
        code, 
        PocketLocalizations.of(context).failedProcessingTransfer,
        ["${PocketLocalizations.of(context).transferError}",exceptionMessage]);
        break;
    case simple_payment_exception: // Problema al leer qr
      _showErrorMessage(context,
        code,
        "${PocketLocalizations.of(context).couldNotReadTheQrImagePleaseTryAgain}",
        [exceptionMessage]);
      break;
    case simple_collect_exception: // Problema al leer qr
      _showErrorMessage(context,
          code,
          "${PocketLocalizations.of(context).couldNotEncryptTheQrPleaseTryAgain}",
          [exceptionMessage]);
      break;
    case simple_movement: // Problema al obtener reporte de simple
      _showErrorMessage(context,
          code,
          "${PocketLocalizations.of(context).theReportCouldNotBeObtainedPleaseTryItLater}",
          [exceptionMessage]);
      break;
    case simple_validate_collect_exception: // Problema al obtener reporte de simple
      _showErrorMessage(context,
          code,
          "${PocketLocalizations.of(context).theValidateCouldNotBeObtainedPleaseTryItLater}",
          [exceptionMessage]);
      break;
  }
}

void _showErrorMessage(BuildContext context, int code, String title, List<String> dialogMesage) {
  var body = dialogMesage.map( (string) => Text(string)).toList();
  body.add(Text("REF: $code"));
  // En los siguientes casos no se cierra la sesión.
  if(code ==no_internet_exception || code == normal_login_exception || code == biometric_login_exception || code == unlink_device_exception){
    DialogUtil.showAcceptDialog(context, title, body, PocketLocalizations.of(context).accept);
  } else{
    DialogUtil.showAcceptDialogWithFunction(context, title, body, (){MainModel.of(context).destroyPayment(); MainModel.of(context).destroyNavigationModel();}, PocketLocalizations.of(context).accept);
  }
}

// Este metodo transforma cualquier objeto a una excepcion conocida
Exception processCatchError(dynamic ex) {
  final logger = SimpleLogger()..mode = LoggerMode.print;
  // Se imprime el tipo de error, por si mañana sale un error desconocido.
  logger.info('processCatchError Error type: ${ex.runtimeType}');
  Exception result;
  if (ex is NoSuchMethodError){ 
    result = null;
  } else if (ex is PosServiceException){
    result = ex;
  } else if (ex is PosException){
    result = ex;
  } else if (ex is DioError) {
      if(ex.type == DioErrorType.DEFAULT && (ex.message.contains("No address associated with hostname") || ex.message.contains("Failed host lookup") || ex.message.contains('Software caused connection abort'))){
        result = PosException("Por favor, revise su conexión a Internet.");
      } else { //FIXME implementar SSL Pinning aqui
        result = PosException(ex.message);
      }
  } else if ( ex is Error) {
    logger.warning(ex.stackTrace);
    result = PosException(Error.safeToString(ex));
  } else if (ex is Exception){
    result = ex;
  } else {
    result = PosException("UN METODO CATCH AGARRO ALGO QUE NO ES EXCEPTION NI ERROR ${ex.runtimeType} - ${ex.toString()} ");
    String message = "SEVERE: processCatchError: No se ha podido determinar el tipo de excepción: ${ex.runtimeType}";
    logger.severe(message);
  }
  return result;
}
class SessionExpiredException implements Exception {
  String _message;

  get message => _message;

  SessionExpiredException(this._message);
}

class PosException implements Exception {
  String _message;

  get message => _message;

  PosException(this._message);
}

class PosServiceException implements Exception {
  PosResponse _response;

  get response => _response;

  PosServiceException(this._response);
}