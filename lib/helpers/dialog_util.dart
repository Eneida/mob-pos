import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mobpos/ui/widget_customized/texts_customized.dart';
import 'dart:io';

class DialogUtil {
  static showAcceptDialog(BuildContext context, String title, List<Widget> children, String accept) {
    showDialog<Null>(
            context: context,
            barrierDismissible: false, // user must tap button!
            builder: (BuildContext context) {
              return new AlertDialog(
                title: new TextCustomized(text: title, style: PocketText.TITLE, fontWeight: FontWeight.bold),
                content: new SingleChildScrollView(
                  child: new ListBody(
                    children: children,
                  ),
                ),
                actions: <Widget>[
                  new FlatButton(
                    child: new Text(accept),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );
  }

  static showAcceptDialogWithFunction(BuildContext context, String title, List<Widget> children, Function fun, String accept) {
    showDialog<Null>(
            context: context,
            barrierDismissible: false, // user must tap button!
            builder: (BuildContext context) {
              return new AlertDialog(
                title: new Text(title),
                content: new SingleChildScrollView(
                  child: new ListBody(
                    children: children,
                  ),
                ),
                actions: <Widget>[
                  new FlatButton(
                    child: new Text(accept),
                    onPressed: fun,
                  ),
                ],
              );
            },
          );
  }
  static showAcceptCancelDialog(BuildContext context, String title, List<Widget> children, Function funCancel, Function funAccept, String accept, String cancel) {
    showDialog<Null>(
            context: context,
            barrierDismissible: false, // user must tap button!
            builder: (BuildContext context) {
              return new AlertDialog(
                title: new Text(title),
                content: new SingleChildScrollView(
                  child: new ListBody(
                    children: children,
                  ),
                ),
                actions: <Widget>[
                  new FlatButton(
                    child: new Text(cancel),
                    onPressed: funCancel,
                  ),
                  new FlatButton(
                    child: new Text(accept),
                    onPressed: funAccept,
                  )
                ],
              );
            },
          );
  }

  static shutdownDialog(BuildContext context, String title, List<Widget> children, String accept) {
    showDialog<Null>(
            context: context,
            barrierDismissible: false, 
            builder: (BuildContext context) {
              return new AlertDialog(
                title: new Text(title),
                content: new SingleChildScrollView(
                  child: new ListBody(
                    children: children,
                  ),
                ),
                actions: <Widget>[
                  new FlatButton(
                    child: new Text(accept),
                    onPressed: () {
                       Navigator.of(context).pop();
                      exit(0);
                    },
                  ),
                ],
              );
            },
          );
  }

  static Future<bool> onBackPressed(BuildContext context, String title, String content, Function function, String page, String yes, String not) {
    return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: TextCustomized(
              text: title,
              style: PocketText.EMPHASIS_TITLE),
          content: Text(content),
          actions: <Widget>[
            FlatButton(
              child: Text(not),
              onPressed: () => Navigator.pop(context, false),
            ),
            FlatButton(
                child: Text(yes),
                onPressed: () {
//                  if(page == Config.new_third_account || page == Config.new_other_bank_account || page == Config.simple_collect || page == Config.simple_payment){
//                    // La sentencia de abajo atrapaba al usuario en la pagina de transferencias.
//                    // Navigator.pushNamedAndRemoveUntil(context, Config.transaction, (Route<dynamic> route) => false);
//                    Navigator.pop(context); // Para remover el dialogo
//                    Navigator.pop(context); // Para volver a la pantalla de selección de cuenta
//                  }else{
//                    Navigator.pushNamedAndRemoveUntil(context, Config.home, (Route<dynamic> route) => false);
//                  }
                  function();
                })
          ],
        ));
  }
}