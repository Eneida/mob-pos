import 'package:flutter/material.dart';
import 'package:mobpos/themes/custom_theme.dart';

class TextFormDefault extends StatelessWidget {
  final String labelTextForm;
  final String validatorText;
  final Map<String, dynamic> formToSave;
  final String valueForm;
  final int maxLines;
  final FocusNode focusNode;
  final TextInputAction textInputAction;
  final Function functionTextKeyBoard;
  final String initialValue;
  final String valueCovered;

  TextFormDefault(
      {@required this.labelTextForm,
      this.validatorText,
      this.formToSave,
      this.valueForm,
      this.maxLines,
      this.focusNode,
      this.textInputAction,
      this.functionTextKeyBoard,
      this.initialValue,
      this.valueCovered});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      maxLines: maxLines == null ? 1 : maxLines,
      autocorrect: false,
      focusNode: focusNode,
      textInputAction: textInputAction,
      initialValue: initialValue,
      style: TextStyle(
        color: initialValue != null? Colors.grey: CustomTheme.getThemeData(context).getAccentColor(),
        fontSize: 20.0,
      ),
      decoration: InputDecoration(
          labelText: labelTextForm,
          filled: false,
          labelStyle: TextStyle(color: CustomTheme.getThemeData(context).getAccentColor(),),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: CustomTheme.getThemeData(context).getAccentColor(),),
          ),
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: CustomTheme.getThemeData(context).getAccentColor()))
      ),
      // ignore: missing_return
      validator: (String value) {
        if (value.isEmpty) {
          return validatorText;
        }
      },
      onSaved: (String value) {
        formToSave[valueForm] = initialValue != null? valueCovered: value;
      },
      onFieldSubmitted: functionTextKeyBoard,
    );
  }
}

class TextsFormPassword extends StatelessWidget {
  final String labelTextForm;
  final Function(String) validationFunction;
  final Map<String, dynamic> formToSave;
  final String valueForm;
  final bool obscureText;
  final Function toggle;
  final FocusNode focusNode;
  final TextInputAction textInputAction;
  final Function functionTextKeyBoard;

  TextsFormPassword({this.labelTextForm, this.validationFunction, this.formToSave,
      this.valueForm, this.obscureText, this.toggle, this.focusNode, this.textInputAction, this.functionTextKeyBoard});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscureText: obscureText,
      focusNode: focusNode,
      textInputAction: textInputAction,
      style: TextStyle(
        color: Colors.white,
        fontSize: 20.0,
      ),
      decoration: InputDecoration(
        labelText: labelTextForm,
        filled: false,
        labelStyle: TextStyle(color: Colors.white,),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white,),
        ),
        enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white)),
        suffixIcon: IconButton(
            icon: Icon(
              Icons.remove_red_eye,
              color: Colors.white,
            ),
            alignment: Alignment.centerRight,
            onPressed: () {
              toggle();
            }),
      ),
      validator: this.validationFunction,
      onSaved: (String value) {
        formToSave[valueForm] = value;
      },
      onFieldSubmitted: functionTextKeyBoard,
    );
  }
}

class TextCustomized extends StatelessWidget {
  final String text;
  final TextAlign textAlign;
  final PocketText style;
  final FontWeight fontWeight;
  final TextOverflow overflow;
  final Color color;

  TextCustomized({
      @required this.text,
      @required this.style,
      this.fontWeight,
      this.textAlign,
      this.overflow,
      this.color
  });
  @override
  Widget build(BuildContext context) {
    //Default style
    var textSize = 20.0;
    var weight = this.fontWeight ?? FontWeight.normal;

    var color = this.color ?? CustomTheme.getThemeData(context).getPrimaryText();
    var padding = EdgeInsets.all(2.0);
    var align = this.textAlign;
    var textDefault = text;
    var overflowText = this.overflow;

    switch (this.style) {
      case PocketText.TITLE:
//        textDefault = textDefault;
        textSize = 20.0;
        weight = this.fontWeight ?? FontWeight.bold;
        color = null;
        padding = EdgeInsets.zero;
        break;

      case PocketText.HEAD_1:
//        textDefault = "Head 1 ${CustomTheme.isIOs(context)}";
//        textDefault = CustomTheme.isIOs(context)
//            ? textDefault
//            : textDefault.toUpperCase();
        textSize = 18.0;
        weight = this.fontWeight ?? FontWeight.bold;
        color = this.color ?? CustomTheme.getThemeData(context).getPrimaryTextNegative();
        padding = EdgeInsets.only(top: 10.0, bottom: 10.0);
        align = this.textAlign ??
            (CustomTheme.isIOs(context) ? TextAlign.left : TextAlign.center);
        break;

      case PocketText.HEAD_2:
        textSize = 15.0;
        weight = this.fontWeight ?? FontWeight.bold;
        color = this.color ??  CustomTheme.getThemeData(context).getPrimaryTextNegative();
        padding = EdgeInsets.only(top: 5.0, bottom: 5.0);
        align = this.textAlign ??
            (CustomTheme.isIOs(context) ? TextAlign.left : TextAlign.center);
        break;

      case PocketText.BODY:
        textSize = 14.0;
        weight = this.fontWeight ?? FontWeight.normal;
        color = this.color ?? CustomTheme.getThemeData(context).getPrimaryText();
        align = this.textAlign != null ? this.textAlign : TextAlign.justify;
        overflowText = this.overflow ?? TextOverflow.clip;
        break;

      case PocketText.EMPHASIS:
        textSize = 14.0;
        weight = this.fontWeight ?? FontWeight.bold;
        color = this.color ?? CustomTheme.getThemeData(context).getAccentColor();
        align = this.textAlign != null ? this.textAlign : TextAlign.left;
        break;

      case PocketText.LABEL:
        textSize = 14.0;
        weight = this.fontWeight ?? FontWeight.bold;
        padding = EdgeInsets.fromLTRB(2.0, 2.0, 10.0, 2.0);
        color = this.color ?? CustomTheme.getThemeData(context).getSecundaryText();
        align = this.textAlign != null ? this.textAlign : TextAlign.left;
        break;

      case PocketText.COMMENT:
        textSize = 12.0;
        weight = this.fontWeight ?? FontWeight.bold;
        padding = EdgeInsets.fromLTRB(2.0, 2.0, 10.0, 2.0);
        color = this.color ?? CustomTheme.getThemeData(context).getSecundaryText();
        align = this.textAlign != null ? this.textAlign : TextAlign.left;
        break;

      case PocketText.EMPHASIS_TITLE:
        textSize = 20.0;
        weight = this.fontWeight ?? FontWeight.bold;
        color = this.color ?? CustomTheme.getThemeData(context).getAccentColor();
        align = this.textAlign != null ? this.textAlign : TextAlign.left;
        break;
    }
    var textWidget = Text(
      textDefault ?? "",
      textAlign: align,
      overflow: overflowText,
      style: TextStyle(fontSize: textSize, fontWeight: weight, color: color),
    );

    var container = Container(
      padding: padding,
      child: textWidget,
    );
    return container;
  }
}

enum PocketText { TITLE, HEAD_1, HEAD_2, BODY, LABEL, EMPHASIS, COMMENT, EMPHASIS_TITLE }
