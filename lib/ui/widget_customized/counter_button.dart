import 'package:flutter/material.dart';
import 'package:mobpos/themes/custom_theme.dart';

class CounterButton extends StatelessWidget{
  final Widget text;
  final Function fun;

  CounterButton(this.text, this.fun);

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      minWidth: 30.0,
      height: 30.0,
      shape: CircleBorder(),
      child: RaisedButton(
        onPressed: fun,
        child: text,
        color: CustomTheme.getThemeData(context).getAccentColor(),
        elevation: 8,
      ),
    );
  }
}