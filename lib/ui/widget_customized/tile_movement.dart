import 'package:flutter/material.dart';
import 'package:mobpos/config.dart';
import 'package:mobpos/dto/response_moviment.dart';
import 'package:mobpos/scoped_models/mainModel.dart';
import 'package:mobpos/themes/custom_theme.dart';

class TileMovement extends StatelessWidget {
  final ResponseMovement movements;
  final MainModel model;
  final int index;

  TileMovement({this.movements, this.model, this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        color: CustomTheme.getThemeData(context).getBackgroundForm(),
        elevation: 5,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(23)),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 13, horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('${movements.dateMovement} ${movements.timeMovement}',
                style: TextStyle(fontSize: 11, color: Color(0xFFAAAAAA)),),
              SizedBox(height: 3.0,),
              Text(movements.trgAccount, style: TextStyle(fontSize: 15),),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(child: Text('${movements.finalDescription}', overflow: TextOverflow.clip,)),
                  SizedBox(width: 8,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text('${movements.finalCurrencyType == 2 ? Config
                          .sus_currency_abr : Config.bs_currency_abr} ${movements
                          .finalAmount}', style: TextStyle(fontSize: 22,
                          fontWeight: FontWeight.w500,
                          color: CustomTheme.getThemeData(context)
                              .getAccentColor()),),
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}