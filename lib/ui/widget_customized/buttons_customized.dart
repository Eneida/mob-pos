import 'package:flutter/material.dart';
import 'package:mobpos/scoped_models/mainModel.dart';
import 'package:mobpos/themes/custom_theme.dart';

class ButtonOutLine extends StatelessWidget {
  final String textButton;
  final Function submitForm;
  final Color colorButton;
  final double textSize;

  ButtonOutLine({@required this.textButton, this.submitForm, this.colorButton, this.textSize});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: ConstrainedBox(
          constraints: BoxConstraints(minWidth: double.infinity),
          child: OutlineButton(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(CustomTheme.getThemeData(context).getButtonRadius())),
            borderSide: BorderSide(width: 1.0, color: colorButton),
            highlightedBorderColor: Colors.orange,
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    textButton,
                    style: TextStyle(
                        fontSize: textSize,
                        color: colorButton,
                        fontWeight: FontWeight.w400),
                  ),
                ],
              )
            ),
            onPressed: submitForm
          )),
    );

//      ButtonTheme(
//      minWidth: 500.0,
//      height: 50.0,
//      buttonColor: CustomTheme.getThemeData(context).getAccentColor(),
//      shape: OutlineInputBorder(
//          borderRadius: BorderRadius.circular(
//              CustomTheme.getThemeData(context).getButtonRadius())),
//      child: RaisedButton(
//        child: Text(textButton,
//            style: TextStyle(
//                fontWeight: FontWeight.bold,
//                fontSize: 16.0,
//                color: Colors.white)),
//        onPressed: submitForm,
//      ),
//    );
  }
}

class ButtonAction extends StatelessWidget {
  final String textButton;
  final String routeButton;
  final bool emphasis;
  final MainModel model;

  ButtonAction(
      {@required this.textButton, this.routeButton, this.model, this.emphasis});
  @override
  Widget build(BuildContext context) {
    Color colorButton;
    Color textColorButton;
    BorderSide borderSide = BorderSide.none;
    if (this.emphasis != null && this.emphasis) {
      colorButton = CustomTheme.getThemeData(context).getEmphasisButtonColor();
      textColorButton =
          CustomTheme.getThemeData(context).getEmphasisButtonTextColor();
    } else {
      colorButton = CustomTheme.getThemeData(context).getButtonColor();
      textColorButton = CustomTheme.getThemeData(context).getButtonTextColor();
      if (CustomTheme.isIOs(context)) {
        borderSide = BorderSide(color: Colors.grey);
      }
    }
    var raised = RaisedButton(
      textColor: textColorButton,
      color: colorButton,  
      child: Text(textButton),
      onPressed: () {
        Navigator.pushReplacementNamed(context, routeButton);
      },
    );
    return ButtonTheme(
      shape: OutlineInputBorder(
          borderSide: borderSide,
          borderRadius: BorderRadius.circular(
              CustomTheme.getThemeData(context).getButtonRadius())),
      child: raised,
    );
  }
}

class ButtonFunction extends StatelessWidget {
  final String textButton;
  final Function functionButton;
  final bool emphasis;

  ButtonFunction(
      {@required this.textButton, this.functionButton, this.emphasis});
  @override
  Widget build(BuildContext context) {
    Color colorButton;
    Color textColorButton;
    BorderSide borderSide = BorderSide.none;
    if (this.emphasis != null && this.emphasis) {
      colorButton = CustomTheme.getThemeData(context).getEmphasisButtonColor();
      textColorButton =
          CustomTheme.getThemeData(context).getEmphasisButtonTextColor();
    } else {
      colorButton = CustomTheme.getThemeData(context).getButtonColor();
      textColorButton = CustomTheme.getThemeData(context).getButtonTextColor();
      if (CustomTheme.isIOs(context)) {
        borderSide = BorderSide(color: Colors.grey);
      }
    }
    var raised = RaisedButton(
      textColor: textColorButton,
      color: colorButton,
      child: Text(textButton),
      onPressed: functionButton,
      disabledColor: CustomTheme.getThemeData(context).getOffSwithColor(),
    );
    return ButtonTheme(
      shape: OutlineInputBorder(
          borderSide: borderSide,
          borderRadius: BorderRadius.circular(
              CustomTheme.getThemeData(context).getButtonRadius())),
      child: raised,
    );
  }
}

class ButtonFormMaterial extends StatelessWidget {
  final String textButton;
  final Function submitForm;
  final MainModel model;

  ButtonFormMaterial({this.textButton, this.submitForm, this.model});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(5.0),
        elevation: 100.0,
        child: MaterialButton(
          minWidth: 500.0,
          height: 50.0,
          onPressed: () {
            submitForm(model);
          },
          color: Color(0xFFF08E33),
          child: Text(
            textButton,
            style: TextStyle(
                color: Colors.white,
                fontSize: 15.0,
                fontWeight: FontWeight.w800),
          ),
        ),
      ),
    );
  }
}

class ButtonFormIcon extends StatelessWidget{
  final String textButton;
  final Function functionButton;
  final bool emphasis;
  final Icon icon;

  ButtonFormIcon({this.textButton, this.functionButton, this.emphasis, this.icon});
  @override
  Widget build(BuildContext context) {
    Color colorButton;
    Color textColorButton;
    BorderSide borderSide = BorderSide.none;
    if (this.emphasis != null && this.emphasis) {
      colorButton = CustomTheme.getThemeData(context).getEmphasisButtonColor();
      textColorButton =
          CustomTheme.getThemeData(context).getEmphasisButtonTextColor();
    } else {
      colorButton = CustomTheme.getThemeData(context).getButtonColor();
      textColorButton = CustomTheme.getThemeData(context).getButtonTextColor();
      if (CustomTheme.isIOs(context)) {
        borderSide = BorderSide(color: Colors.grey);
      }
    }
    var raised = RaisedButton(
      textColor: textColorButton,
      color: colorButton,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Center(child: Text(textButton),),
          icon
        ],),
      onPressed: functionButton,
      disabledColor: CustomTheme.getThemeData(context).getOffSwithColor(),
    );
    return ButtonTheme(
      shape: OutlineInputBorder(
          borderSide: borderSide,
          borderRadius: BorderRadius.circular(
              CustomTheme.getThemeData(context).getButtonRadius())),
      child: raised,
    );
  }
}