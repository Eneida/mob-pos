import 'package:flutter/material.dart';

class TileProduct extends StatefulWidget{
  final int index;

  TileProduct(this.index);
  @override
  State<StatefulWidget> createState() {
    return _TileProductState();
  }
}

class _TileProductState extends State<TileProduct>{
  List<String> fetchPost;
  List<bool> _isExpanded;

  @override
  Widget build(BuildContext context) {
    return ExpansionPanelList(
        animationDuration: Duration(milliseconds: 500),
        expansionCallback: (int index, bool isExpanded) {
          setState(() {
            _isExpanded[index] = !isExpanded;
            print("INDEX : $index ... $isExpanded");
          });
        },
        children: List());
  }

  void expandList(int length) {
    if (_isExpanded == null) {
      _isExpanded = new List(length);
      _isExpanded.fillRange(0, length - 1, false);
    }
  }

  List<ExpansionPanel> buildPanelList(List<String> data) {
    List<ExpansionPanel> children = List<ExpansionPanel>();
    for (int i = 0; i < data.length; i++) {
      children.add(ExpansionPanel(
        headerBuilder: (context, isExpanded) {
          return Text(" my tile number was $i");
        },
        isExpanded: _isExpanded[i] ?? false,
        body: Container(
          color: Colors.black,
        ),
      ));
    }
    return children;
  }
}