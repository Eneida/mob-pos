import 'package:flutter/material.dart';

class CardCustomized extends StatelessWidget{

  final Widget child;
  final EdgeInsets margin;
  final EdgeInsets padding;
  final double elev;
  final Color color;

  CardCustomized({@required this.child, this.margin, this.padding, this.elev, this.color});

  @override
  Widget build(BuildContext context) {
    var padding = this.padding ?? EdgeInsets.all(10.0);
    var card = Card(
        color: color,
        elevation: elev,
        child: Container(
          child: child,
          padding: padding,
        ),
        margin: this.margin ?? EdgeInsets.zero,
        
    );
    return card;
  }
}

