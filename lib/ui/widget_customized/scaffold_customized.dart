import 'package:flutter/material.dart';
import 'package:mobpos/themes/custom_theme.dart';

class ScaffoldCustomized extends StatelessWidget{

  final AppBar appBar;
  final Widget body;
  final Widget bottomNavigationBar;
  final bool resizeToAvoidBottomPadding;

  ScaffoldCustomized({@required this.body, this.appBar, this.bottomNavigationBar, this.resizeToAvoidBottomPadding=true});

  @override
  Widget build(BuildContext context) {
//    String sessionBackground = MainModel.of(context).sessionBackground;
    EdgeInsets padding;

    if (CustomTheme.isIOs(context)) {
      padding = EdgeInsets.only(left: 10.0, right: 10.0);
    } else {
      padding = EdgeInsets.only(left: 10.0, right: 10.0);
    }
    var scaffold = Scaffold(
      primary: true,
      resizeToAvoidBottomPadding: this.resizeToAvoidBottomPadding,
      //backgroundColor: CustomTheme.getThemeData(context).getScaffoldBackground(),
      appBar: this.appBar,
//      body: Stack(
//        children: <Widget>[
      body: Container(
        //    decoration: BoxDecoration(
        //      image: DecorationImage(
        //        image: AssetImage(sessionBackground),
        //        colorFilter: ColorFilter.mode(Config.backgroundColor.withOpacity(Config.overlay), BlendMode.hardLight),
        //        fit: BoxFit.cover,
        //      ),
        //    ),
            padding: padding,
            child: body,
          ),
//        ],
//      ),
      bottomNavigationBar: this.bottomNavigationBar ,
    );
    return scaffold;
  }
}
