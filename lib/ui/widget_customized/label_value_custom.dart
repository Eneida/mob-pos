import 'package:flutter/material.dart';
import 'package:mobpos/ui/widget_customized/texts_customized.dart';

class LabelValueCustom extends StatelessWidget {
  final String label;
  final String value;

  LabelValueCustom(this.label, this.value);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          TextCustomized(
              style: PocketText.EMPHASIS,
              text: this.label,
              overflow: TextOverflow.clip,
          ),
          TextCustomized(
              style: PocketText.BODY,
              text: this.value,
              textAlign: TextAlign.left,
              overflow: TextOverflow.clip)
        ],
      ),
    );
  }
}
