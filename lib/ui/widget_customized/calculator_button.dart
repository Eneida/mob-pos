import 'package:flutter/material.dart';
import 'package:mobpos/themes/custom_theme.dart';

class CalculatorButton<T> extends StatelessWidget {
  final T number;
  final Key keyNumber;
  final Function keyTap;
  final Color color;

  CalculatorButton({this.number, this.keyNumber, this.keyTap, this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(2.0),
      width: 60.0,
      height: 60.0,
      child: RaisedButton(
        elevation: 8.0,
        splashColor: Color(0xFFC5CAE9),
        color: Color(0xFFE8EAF6),
        highlightElevation: 16,
        //color: Colors.white,
        shape: CircleBorder(),
        //highlightColor: Color.alphaBlend(Color(0xFF9FA8DA), Colors.yellow),
        onPressed: (){
          keyTap(keyNumber);
        },
        child: number.runtimeType == String
            ? Text(number as String,
          style: TextStyle(fontSize: 35.0, fontWeight: FontWeight.bold, color: CustomTheme.getThemeData(context).getAccentColor()),
        ): number,
      ),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
       // border: Border.all(color: Color(0xFF5C6BC0)),
      ),
    );
  }
}
