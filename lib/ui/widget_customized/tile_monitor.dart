import 'package:flutter/material.dart';

class TileMonitor extends StatelessWidget{
  final String idTransfer;
  final String originName;
  final String amount;

  TileMonitor({this.idTransfer, this.originName, this.amount});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Text('Id transferencia: $idTransfer'),
          Row(children: <Widget>[
            Text('Originante: $originName'),
            Text('Monto: $amount'),
        ],)
        ],
      ),
    );
  }
}