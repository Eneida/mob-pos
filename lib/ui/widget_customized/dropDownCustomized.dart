import 'package:flutter/material.dart';
import 'package:mobpos/themes/custom_theme.dart';

class DropDownCustomized<T> extends StatelessWidget{
  final T itemSelected;
  final String _hintText;
  final List<DropdownMenuItem<T>> dropDownList;
  final Function(T) _onChanged;

  DropDownCustomized(this.itemSelected, this._hintText, this.dropDownList, this._onChanged);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: (_hintText == 'Mes'||_hintText == 'Año')?BoxDecoration(
        border: Border.all(
            color: CustomTheme.getThemeData(context).getDividerColor()),
      ):BoxDecoration(),
      child: DropdownButtonHideUnderline(
        child: ButtonTheme(
          alignedDropdown: true,
          child: DropdownButton<T>(
            hint: Text(_hintText),
            value: itemSelected,
            items: dropDownList,
            onChanged: _onChanged,
          ),
        ),
      ),
    );
  }
}