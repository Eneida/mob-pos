import 'package:flutter/material.dart';

class Loading extends StatelessWidget {
  final String text;

  Loading(this.text);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
 //       Image(image: AssetImage("assets/loading_med.gif"), height: 75.0, width: 75.0,),
        Image(image: AssetImage("assets/loading.gif"), height: 75.0, width: 75.0,),
        Text(text, style: TextStyle(color: Colors.grey), textAlign: TextAlign.center)
      ],
    ));
  }
}
