import 'package:firebase_database/firebase_database.dart';

class ProductDto {
  String key;
  String idTransfer;
  String amount;
  String currency;
  String destination;
  String account;
  String date;
  bool isPay;


  ProductDto({this.idTransfer, this.amount, this.currency, this.destination, this.account, this.date, this.isPay});

  ProductDto.map(dynamic obj) {
    this.key = obj['key'];
    this.idTransfer = obj['idTransfer'];
    this.amount = obj['amount'];
    this.currency = obj['currency'];
    this.destination = obj['destination'];
    this.account = obj['account'];
    this.date = obj['date'];
    this.isPay = obj['isPay'];
  }

  ProductDto.fromJson(Map<dynamic, dynamic> json) {
    key = json['key'];
    idTransfer = json['idTransfer'];
    amount = json['amount'];
    currency =  json['currency'];
    destination = json['destination'];
    account = json['account'];
    date = json['date'];
    isPay = json['isPay'];
  }

  ProductDto.fromSnapshot(DataSnapshot snapshot) {
    key = snapshot.key;
    idTransfer = snapshot.value['idTransfer'];
    amount = snapshot.value['amount'];
    currency = snapshot.value['currency'];
    destination = snapshot.value['destination'];
    account = snapshot.value['account'];
    date = snapshot.value['date'];
    isPay = snapshot.value['isPay'];
  }

  toJson () {
    return {
      "key": key,
      "idTransfer": idTransfer,
      "amount": amount,
      "currency": currency,
      "destination": destination,
      "account": account,
      "date": date,
      "isPay": isPay,
    };
  }

  ProductDto.fromJson2(this.key, Map data) {
    key = data['idTransfer'];
    idTransfer = data['idTransfer'];
    amount = data['amount'];
    currency = data['currency'];
    destination = data['destination'];
    account = data['account'];
    date = data['date'];
    isPay = data['isPay'];
  }

  String toString(){
    return "ProductDto {"
        "key: $key, "
        "idTransfer: $idTransfer, "
        "amount: $amount, "
        "currency: $currency, "
        "destination: $destination, "
        "account: $account, "
        "data: $date, "
        "isPay: $isPay, "
        "}";
  }
}