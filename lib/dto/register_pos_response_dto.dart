import 'dart:convert';

class RegisterPosResponseDto{
  final String holderName;
  final String company;
  final String mobileNumber;

  RegisterPosResponseDto(this.holderName, this.company, this.mobileNumber);

  RegisterPosResponseDto.fromJson(Map<String, dynamic> json)
    : holderName = json['holderName'],
      company = json['company'],
      mobileNumber = json['mobileNumber'];

  Map<String, dynamic> toJson() => <String, dynamic>{
    'holderName': holderName,
    'company': company,
    'mobileNumber': mobileNumber
  };

  String toString(){
    return 'RegisterPosResponseDto { '
        'holderName: $holderName, '
        'company: $company, '
        'mobileNumber: $mobileNumber '
        '}';
  }
}

RegisterPosResponseDto registerPosFromJson(String str){
  final jsonData = json.decode(str);
  return RegisterPosResponseDto.fromJson(jsonData);
}