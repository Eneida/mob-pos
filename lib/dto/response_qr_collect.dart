class ResponseQrCollect{
  final int qrId;

  ResponseQrCollect(this.qrId);

  ResponseQrCollect.fromJson(Map<String, dynamic> json)
  : qrId = json['qrId'];

  String toString(){
    return 'ResponseQRCollect { '
        'qrId: $qrId '
        '}';
  }
}