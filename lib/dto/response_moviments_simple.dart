class ResponseMovementSimple {
  final String movementId;
  final String dateMovement;
  final String timeMovement;
  final String srcAccount;
  final String srcName;
  final String srcDocumentId;
  final String trgAccount;
  final String trgName;
  final String trgDocumentId;
  final String src_bank_eif;
  final String src_bank_name;
  final String trgCodeEif;
  final String trgNameEif;
  final String expireDate;
  final String expirePeriod;
  final bool expiredTransaction;
  final int singleUse;
  final String requestAmount;
  final int requestCurrencyType;
  final String finalAmount;
  final int finalCurrencyType;
  final String description;
  final String finalDescription;
  final bool collectTransfer;
  final String qrCode;
  final String idQr;
  final bool variableAmount;

  ResponseMovementSimple({
    this.movementId,
    this.dateMovement,
    this.timeMovement,
    this.srcAccount,
    this.srcName,
    this.srcDocumentId,
    this.trgAccount,
    this.trgName,
    this.trgDocumentId,
    this.src_bank_eif,
    this.src_bank_name,
    this.trgCodeEif,
    this.trgNameEif,
    this.expireDate,
    this.expirePeriod,
    this.expiredTransaction,
    this.singleUse,
    this.requestAmount,
    this.requestCurrencyType,
    this.finalAmount,
    this.finalCurrencyType,
    this.description,
    this.finalDescription,
    this.collectTransfer,
    this.qrCode,
    this.idQr,
    this.variableAmount
  });

  ResponseMovementSimple.fromJson(Map<String, dynamic> json)
    :   movementId = json['movementId'],
        dateMovement = json['dateMovement'],
        timeMovement = json['timeMovement'],
        srcAccount = json['srcAccount'],
        srcName = json['srcName'],
        srcDocumentId = json['srcDocumentId'],
        trgAccount = json['trgAccount'],
        trgName = json['trgName'],
        trgDocumentId = json['trgDocumentId'],
        src_bank_eif = json['src_bank_eif'],
        src_bank_name = json['src_bank_name'],
        trgCodeEif = json['trgCodeEif'],
        trgNameEif = json['trgNameEif'],
        expireDate = json['expireDate'],
        expirePeriod = json['expirePeriod'],
        expiredTransaction = json['expiredTransaction'],
        singleUse = json['singleUse'],
        requestAmount = json['requestAmount'],
        requestCurrencyType = json['requestCurrencyType'],
        finalAmount = json['finalAmount'],
        finalCurrencyType = json['finalCurrencyType'],
        description = json['description'],
        finalDescription = json['finalDescription'],
        collectTransfer = json['collectTransfer'],
        qrCode = json['qrCode'],
        idQr = json['idQr'],
        variableAmount = json['variableAmount'];

  String toString(){
    return "ResponseMovimentSimple {"
        "movementId: $movementId, "
        "dateMovement: $dateMovement, "
        "timeMovement: $timeMovement, "
        "srcAccount: $srcAccount, "
        "srcName: $srcName, "
        "srcDocumentId: $srcDocumentId, "
        "trgAccount: $trgAccount, "
        "trgName: $trgName, "
        "trgDocumentId: $trgDocumentId, "
        "src_bank_eif: $src_bank_eif, "
        "src_bank_name: $src_bank_name, "
        "trgCodeEif: $trgCodeEif, "
        "trgNameEif: $trgNameEif, "
        "expireDate: $expireDate, "
        "expirePeriod: $expirePeriod, "
        "expiredTransaction: $expiredTransaction, "
        "singleUse: $singleUse, "
        "requestAmount: $requestAmount, "
        "requestCurrencyType: $requestCurrencyType, "
        "finalAmount: $finalAmount, "
        "finalCurrencyType: $finalCurrencyType, "
        "description: $description, "
        "finalDescription: $finalDescription, "
        "collectTransfer: $collectTransfer, "
        "qrCode: $qrCode, "
        "idQr: $idQr, "
        "variableAmount: $variableAmount, "
        "}";
  }
}