import 'dart:convert';

class PosResponse {
  final String statusCode;
  final dynamic response; // La respuesta como JSON
  final String errorDetail;

  PosResponse({this.statusCode, this.response, this.errorDetail});

  PosResponse.fromJson(Map<String, dynamic> json)
      : statusCode = json['statusCode'],
        response = json['response'],
        errorDetail = json['errorDetail'];

  get status => statusCode.endsWith("-0000");
}

PosResponse posResponseFromJson(String str){
  final jsonData = json.decode(str);
  return PosResponse.fromJson(jsonData);
}
