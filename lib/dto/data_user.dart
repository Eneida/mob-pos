import 'package:mobpos/config.dart';
import 'package:mobpos/helpers/secure_storage.dart';

class DataUser{
  String ac;
  String di;
  String name;
  String welcomePage;

  DataUser(this.ac, this.di, this.name);

  DataUser.fromJson(Map<String, dynamic> json)
      : ac = json['ac'],
        di = json['di'],
        name = json['name'];

  Map<String, dynamic> toJson() => <String, dynamic>{
    'ac': ac,
    'di': di,
    'name': name,
  };

  String toString(){
    return 'DataUser { '
        'ac: $ac, '
        'di: $di, '
        'name: $name '
        '}';
  }

  static Future<String> initPage() async {
    DataUser dataUser = DataUser('', '', '');
    String token = await SecureStorage.read(Config.jwt_token_key).then( (value) => value);
    if(token != null){
      dataUser.welcomePage = Config.calculator_page;
    }else {
      dataUser.welcomePage = Config.welcome_page;
    }
    print('Pantalla inicial:::. ${dataUser.welcomePage}');

    return dataUser.welcomePage;
  }
}