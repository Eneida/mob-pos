class ResponseMonitorSimple {
  final String idTransfer;
  final String originName;
  final String amount;

  ResponseMonitorSimple({this.idTransfer, this.originName, this.amount});

  String toString(){
    return "ResponseMonitorSimple {"
        "idTransfer: $idTransfer, "
        "originName: $originName, "
        "amount: $amount, "
        "}";
  }
}